from ratClass import alusinusRat, babylonRat, bxRat, coringaRat, ctOsRat, greameRat, imminentRat, imperiumRat, l6Rat, ningaliNetRat, novaLiteRat, quasarRat, revengeRat, spyGateRat, vanToMRat
from datetime import datetime
from config import inc
from utils import phf_state
import generate_result
import subprocess
import threading
import os
import signal
import time
import paramiko
import traceback
import json

class main_test():
    # def __init__(self):
    #     self.ssh = paramiko.SSHClient()
    #     self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    #     self.ssh.connect(**inc.detector_ssh_config)

    def run_runner(self):
        process = subprocess.Popen("Runner.exe", stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        self.runner_pid = process.pid
        # for line in iter(process.stdout.readline, ''):
        #     print(line.decode('utf-8'))

    def run_trace_event(self):
        process = subprocess.Popen(r"PowerShell\TraceEvent2.exe --provider=Microsoft-Windows-Powershell --mode=s -r")
        self.trace_event_pid = process.pid

    def get_rats_list(self):
        res = []
        if "alusinusRat" in inc.rat_list:
            res.append(alusinusRat.AlusinusRat)
        if "babylonRat" in inc.rat_list:
            res.append(babylonRat.BabylonRat)
        if "bxRat" in inc.rat_list:
            res.append(bxRat.BXRat)
        if "coringaRat" in inc.rat_list:
            res.append(coringaRat.CoringaRat)
        if "ctOsRat" in inc.rat_list:
            res.append(ctOsRat.CtOsRat)
        if "greameRat" in inc.rat_list:
            res.append(greameRat.GreameRat)
        if "imminentRat" in inc.rat_list:
            res.append(imminentRat.ImminentRat)
        if "imperiumRat" in inc.rat_list:
            res.append(imperiumRat.ImperiumRat)
        if "l6Rat" in inc.rat_list:
            res.append(l6Rat.L6Rat)
        if "ningaliNetRat" in inc.rat_list:
            res.append(ningaliNetRat.NingaliNetRat)
        if "novaLiteRat" in inc.rat_list:
            res.append(novaLiteRat.NovaLiteRat)
        if "quasarRat" in inc.rat_list:
            res.append(quasarRat.QuasarRat)
        if "revengeRat" in inc.rat_list:
            res.append(revengeRat.RevengeRat)
        if "spyGateRat" in inc.rat_list:
            res.append(spyGateRat.SpyGateRat)
        if "vanToMRat" in inc.rat_list:
            res.append(vanToMRat.VanToMRat)
        return res

    def write_res_to_file(self, rat_name, remote_desktop_res, remote_shell_res, keylogger_res, download_and_execute_res):
        with open(self.res_file_name, 'a+') as file:
            file.write("""
{}: 
    remote_desktop: {}
    remote_shell: {}
    keylogger: {}
    download_and_execute: {}
            """.format(rat_name, remote_desktop_res, remote_shell_res, keylogger_res, download_and_execute_res))

    def run_rats(self, remote_desktop, remote_shell, keylogger, download_and_execute):
        rats = self.get_rats_list()
        res = []
        for rat_class in rats:
            res_dict = {}
            try:
                rat = rat_class()
                pid = rat.server_pid
            except:
                print (traceback.format_exc())
                continue
            remote_desktop_res, remote_shell_res, keylogger_res, download_and_execute_res = rat.start_test(remote_desktop, remote_shell, keylogger, download_and_execute)
            # self.write_res_to_file(rat.__class__.__name__, remote_desktop_res, remote_shell_res, keylogger_res, download_and_execute_res)
            res_dict["rat_name"] = rat.__class__.__name__
            res_dict["pid"] = pid
            res_dict["result"] = {
                "remote_desktop": remote_desktop_res,
                "remote_shell": remote_shell_res,
                "keylogger": keylogger_res,
                "download_and_execute": download_and_execute_res
            }
            res.append(res_dict)
        return res


    def do_test(self, remote_desktop, remote_shell, keylogger, download_and_execute):
        self.res_file_name = "{}_{}.txt".format(inc.output_file_name, datetime.now().strftime("%Y-%m-%d_%H-%M-%S"))
        # self.config_path()
        #     remote_shell: {}self.run_detector()
        # self.trace_event_thread = threading.Thread(target=self.run_trace_event)
        # self.runner_thread = threading.Thread(target=self.run_runner)
        # self.trace_event_thread.start()
        # self.runner_thread.start()
        # time.sleep(60)
        test_res = self.run_rats(remote_desktop, remote_shell, keylogger, download_and_execute)
        time.sleep(30)

        os.system('taskkill /F /IM ClientSchedule.exe')
        os.system('taskkill /F /IM ETWDataCollectorRedesigned.exe')
        traces = generate_result.get_traces(test_res)
        # detector_res = generate_result.get_detector_result()
        # for each in test_res:
        #     for phf_name in each['result'].keys():
        #         if each['result'][phf_name]==phf_state.success and phf_name not in detector_res[each['pid']]:
        #             each['result'][phf_name] = phf_state.not_detected
        for each in test_res:
            if each['pid'] in traces.keys():
                each['traces'] = traces[each['pid']]
        generate_result.simple_detect(test_res)

        detector_res = generate_result.get_detector_result(test_res)
        print (detector_res)
        for each in test_res:
            if each['pid'] in detector_res.keys():
                each['detector_res'] = detector_res[each['pid']]
        # print (json.dumps(traces))
        # print (json.dumps(test_res))
        path = os.getcwd()
        # os.chdir(r"../../")
        with open(self.res_file_name, 'a+') as file:
            file.write(json.dumps(test_res, indent=4))
        os.chdir(path)
        generate_result.generate_table(test_res)


        # time.sleep(5)
        # self.kill_collector()
        # self.kill_detector()



    def start_test_with_config_file(self, file_name):
        pass


    def kill_collector(self):
        subprocess.Popen("taskkill /pid {} /f".format(self.runner_pid))
        subprocess.Popen("taskkill /pid {} /f".format(self.trace_event_pid))
        subprocess.Popen("taskkill /IM \"ETWDataCollectorRedesigned.exe\" /f".format(self.trace_event_pid))
        # os.kill(self.runner_pid, signal.SIGTERM)
        # os.kill(self.trace_event_pid, signal.SIGTERM)

    def config_path(self):
        base_dir = os.getcwd()
        dir = base_dir + r"\resources\TA1-Binaries-master"
        os.chdir(dir)

    # def run_detector(self):
    #     self.ssh.exec_command("nohup ./run.sh >nohup.out 1>&1&")
    #
    # def kill_detector(self):
    #     self.ssh.connect(**inc.detector_ssh_config)
    #     self.ssh.exec_command("ps -ef | grep "+inc.detector_ssh_config["username"]+" | grep Detector.jar | grep java | awk '{print $2}' | xargs kill")
    #     self.ssh.exec_command("mv detectionDetails/ detectionDetails_{}".format(datetime.now().strftime("%Y-%m-%d_%H:%M:%S")))


def main():
    test = main_test()
    test.do_test(True, True, True, True)

if __name__ == '__main__':
    main()