from ratClass.rat import Rat
from config import inc
from pywinauto.application import Application
from utils.phf_operation import retry_if_fail, not_implement
from utils import get_logger
import subprocess
import threading
import time
import os

logger = get_logger.get_common_logger()


class AlusinusRat(Rat):
    version = "0.8"
    def __init__(self):
        logger.info("start testing {}".format(self.__class__.__name__))
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        self.app = Application(backend="uia").start(os.path.abspath(r'resources\Alibaba-Cloud-Guard-RATs\Rat\Alusinus RAT v0.8\Alusinus RAT v0.8\Alusinus.exe'))
        # self.dlg = self.app.window(auto_id="Main")

    def server_start(self):
        self.server_start_time = time.time()
        self.server = subprocess.Popen(os.path.abspath(r"resources\Alibaba-Cloud-Guard-RATs\Rat\Alusinus RAT v0.8\Alusinus RAT v0.8\Server.exe"), shell=False)
        self.server_pid = self.server.pid
        logger.debug("alusinus rat server pid: {}".format(self.server.pid))

    @retry_if_fail(inc.rats_retry_times)
    def remote_desktop(self):
        self.dlg = self.app.window(title="RAT Alusinus 0.8")
        self.dlg.set_focus()
        self.dlg.child_window(control_type="List").ListItem.wait('exists', timeout=120)
        self.dlg.child_window(control_type="List").ListItem.click_input(button='right')
        # time.sleep(1)
        self.dlg = self.app.window(title="上下文", control_type = "Menu")
        self.dlg.MenuItem8.select()

        self.dlg = self.app.window(class_name = "TForm9")
        self.dlg.click_input(button='right')
        self.dlg = self.app.window(title="上下文", control_type = "Menu")
        self.dlg.MenuItem.select()
        time.sleep(inc.remote_desktop_sleep_time)
        self.app.window(class_name="TForm9").close()

    @retry_if_fail(inc.rats_retry_times)
    def key_logger(self):
        self.dlg = self.app.window(title="RAT Alusinus 0.8")
        self.dlg.set_focus()
        while not self.dlg.child_window(control_type="List").ListItem.exists():
            time.sleep(0.1)
        self.dlg.child_window(control_type="List").ListItem.click_input(button='right')
        self.dlg = self.app.window(title="上下文", control_type="Menu")
        self.dlg.MenuItem10.select()
        self.dlg = self.app.window(class_name="TForm11")
        self.dlg.Iniciar.click()
        self.dlg.type_keys(inc.keylogger_command)
        self.dlg.child_window(control_type = "StatusBar").click_input(button='left')
        self.dlg.Detener.click()
        self.dlg.close()

    @retry_if_fail(inc.rats_retry_times)
    def remote_shell(self):
        self.dlg = self.app.window(title="RAT Alusinus 0.8")
        self.dlg.set_focus()
        while not self.dlg.child_window(control_type="List").ListItem.exists():
            time.sleep(0.1)
        self.dlg.child_window(control_type="List").ListItem.click_input(button='right')
        self.dlg = self.app.window(title="上下文", control_type="Menu")
        self.dlg.MenuItem11.select()
        self.dlg = self.app.window(class_name = "TForm12")
        self.dlg.Activar.click()
        for cmd in inc.remote_shell_command:
            self.dlg.child_window(class_name="TEdit").set_text(cmd)
            self.dlg.Enviar.click()
            time.sleep(1)
        self.dlg.close()

    @not_implement
    def download_execute(self):
        pass

    # def start_test(self):
    #     self.server_start()
    #     self.remote_desktop()
    #     self.key_logger()
    #     self.remote_shell()
    #     self.close_software()

    def close_software(self):
        self.server.terminate()
        try:
            self.app.kill()
        except:
            os.system("taskkill /F /PID {}".format(self.app.process))
        os.system("taskkill /F /IM Server.exe")
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))



if __name__ == '__main__':
    os.chdir(os.path.abspath(os.path.join(os.getcwd(), "../resources/TA1-Binaries-master")))
    rat = AlusinusRat()
    rat.start_test(True, True, True, True)
    # rat.start_test(False, False, False, False)