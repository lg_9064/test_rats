from ratClass.rat import Rat
from pywinauto.application import Application
import subprocess
import threading
import time

# useless
class UculRat(Rat):
    version = "1.1"

    def __init__(self):
        self.app = Application(backend="uia").start(r'..\Alibaba-Cloud-Guard-RATs\Rat\ucuL v1.1\ucuL v1.1\ucul.exe')

    def server_start(self):
        self.server = subprocess.Popen(r"..\Alibaba-Cloud-Guard-RATs\Rat\ucuL v1.1\ucuL v1.1\Server.exe", shell=False)
        self.server_pid = self.server.pid
        print ("ucul rat server pid: {}".format(self.server.pid))

    def remote_desktop(self):
        self.dlg = self.app.window(title="ucuL v1.1")
        # self.dlg.print_control_identifiers()
        while not self.dlg.child_window(control_type="List").ListItem.exists():
            time.sleep(0.1)
        self.dlg.child_window(control_type="List").ListItem.click_input(button = 'right')
        self.dlg = self.app.window(title="上下文")
        # self.dlg.Main.items()[0].click()
        print (self.dlg.Main.items())
        self.app.top_window().click()
        self.dlg.print_control_identifiers()
        # self.app.top_window().print_control_identifiers()
        # self.dlg.Main.ScreenCapture.select()


    def close_software(self):
        self.app.kill()
        self.server.terminate()

    def start_test(self):
        self.server_start()
        rat.remote_desktop()
        # self.key_logger()
        # self.remote_shell()
        # self.close_software()

if __name__ == '__main__':
    rat = UculRat()
    rat.start_test()