from ratClass.rat import Rat
from pywinauto.application import Application
from utils.phf_operation import retry_if_fail
from utils import get_logger
from config import inc
import subprocess
import threading
import time
import os

logger = get_logger.get_common_logger()

class BabylonRat(Rat):
    version = "1.5.1.0"

    def __init__(self):
        logger.info("start testing {}".format(self.__class__.__name__))
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        print (os.path.abspath(r"..\Alibaba-Cloud-Guard-RATs\Rat\Babylon 1.5.1.0\server.exe"))
        self.server_start_time = time.time()
        self.server = subprocess.Popen(os.path.abspath(r"resources\Alibaba-Cloud-Guard-RATs\Rat\Babylon 1.5.1.0\server.exe"), shell=False)
        self.server_pid = self.server.pid
        logger.debug("babylon rat server pid: {}".format(self.server.pid))
        self.app = Application(backend="uia").start(os.path.abspath(r'resources\Alibaba-Cloud-Guard-RATs\Rat\Babylon 1.5.1.0\Babylon.exe'))
        self.dlg = self.app.window(auto_id="Main")

    def server_start(self):
        self.dlg.set_focus()
        self.dlg.child_window(auto_id='evolveThemeControl1').menu_select('File->Server->Start')

    @retry_if_fail(inc.rats_retry_times)
    def remote_desktop(self):
        self.dlg = self.app.window(auto_id="Main")
        self.dlg.click_input(button='left')
        self.dlg.child_window(auto_id='evolveThemeControl1').child_window(auto_id='evolveTabControl1').child_window(
            auto_id="tabPage3").child_window(auto_id="lstClients").ListItem.click_input(button='right')
        self.dlg.child_window(auto_id="menuMain").Surveillance.select()
        self.dlg.child_window(auto_id="menuMain").Surveillance.RemoteDesktop.select()
        # dlg.print_control_identifiers()

        # 切换窗口
        self.dlg = self.app.window(auto_id="RemoteDesktop")
        # dlg.print_control_identifiers()
        self.dlg.child_window(auto_id="barControls").Stream.click()
        # self.dlg.child_window(auto_id="barControls").Keyboard.click()
        time.sleep(inc.remote_desktop_sleep_time)
        self.dlg.close()
        # self.dlg.type_keys("123321")

        # self.dlg.print_control_identifiers()

    @retry_if_fail(inc.rats_retry_times)
    def key_logger(self):
        self.dlg = self.app.window(auto_id="Main")
        self.dlg.click_input(button='left')
        self.dlg.child_window(auto_id='evolveThemeControl1').child_window(auto_id='evolveTabControl1').child_window(
            auto_id="tabPage3").child_window(auto_id="lstClients").ListItem.click_input(button='right')
        self.dlg.child_window(auto_id="menuMain").Surveillance.select()
        self.dlg.child_window(auto_id="menuMain").Surveillance.Keylogger.select()

        self.dlg = self.app.window(auto_id="Offline", control_type="Window")
        self.dlg.child_window(auto_id="evolveThemeControl1").child_window(auto_id="evolveGroupBox1").Enable.click_input(button='left')
        self.dlg.child_window(auto_id="2").click()
        self.dlg.type_keys(inc.keylogger_command)
        self.dlg.child_window(auto_id="evolveThemeControl1").child_window(auto_id="evolveGroupBox1").Disable.click_input(
            button='left')
        self.dlg.child_window(auto_id="2").click()
        self.dlg.close()
        # self.dlg.print_control_identifiers()

    @retry_if_fail(inc.rats_retry_times)
    def remote_shell(self):
        self.dlg = self.app.window(auto_id="Main")
        self.dlg.click_input(button='left')
        self.dlg.child_window(auto_id='evolveThemeControl1').child_window(auto_id='evolveTabControl1').child_window(
            auto_id="tabPage3").child_window(auto_id="lstClients").ListItem.click_input(button='right')
        self.dlg.child_window(auto_id="menuMain").System.select()
        self.dlg.child_window(auto_id="menuMain").System.RemoteShell.select()

        self.dlg = self.app.window(auto_id="RemoteShell")
        for cmd in inc.remote_shell_command:
            self.dlg.child_window(auto_id="evolveThemeControl1").Edit.set_text(cmd)
            self.dlg.child_window(auto_id="evolveThemeControl1").child_window(auto_id="evolveButton1").click_input(button='left')
            time.sleep(1)
        self.dlg.close()
        # self.app.top_window().print_control_identifiers()

    @retry_if_fail(inc.rats_retry_times)
    def download_execute(self):
        self.dlg = self.app.window(auto_id="Main")
        # self.dlg.print_control_identifiers()
        self.dlg.click_input(button='left')
        self.dlg.child_window(auto_id='evolveThemeControl1').child_window(auto_id='evolveTabControl1').child_window(
            auto_id="tabPage3").child_window(auto_id="lstClients").ListItem.click_input(button='right')
        self.dlg.child_window(auto_id="menuMain").System.select()
        self.dlg.child_window(auto_id="menuMain").System.child_window(best_match="Download/Execute").click_input(button="left")
        self.dlg = self.app.window(title = "Download/Execute")
        self.dlg.child_window(auto_id="evolveThemeControl1").child_window(auto_id="txtInput").set_text(inc.download_and_execute_file_url)
        self.dlg.child_window(auto_id="evolveThemeControl1").child_window(auto_id="evolveButton1").click_input(button="left")

    def close_software(self):
        try:
            self.app.kill()
        except:
            os.system("taskkill /F /PID {}".format(self.app.process))
        self.server.terminate()
        os.system("taskkill /F /IM server.exe")
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))


    # def start_test(self):
    #     self.server_start()
    #     self.key_logger()
    #     self.remote_shell()
    #     self.download_execute()
    #     self.remote_desktop()
    #     self.close_software()


if __name__ == '__main__':
    os.chdir(os.path.abspath(os.path.join(os.getcwd(), "../resources/TA1-Binaries-master")))
    rat = BabylonRat()
    rat.start_test(True, True, True, True)