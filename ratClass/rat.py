from utils.phf_operation import not_implement, restore_path
from utils import phf_state
from abc import abstractmethod
from utils import get_logger
import traceback
import time
import os

logger = get_logger.get_common_logger()

class Rat(object):
    def __init__(self):
        pass

    def server_start(self):
        pass

    @abstractmethod
    def remote_desktop(self):
        pass

    @abstractmethod
    def key_logger(self):
        pass

    @abstractmethod
    def remote_shell(self):
        pass

    @abstractmethod
    def download_execute(self):
        pass


    @abstractmethod
    def close_software(self):
        pass

    @restore_path
    def write_res(self, res):
        # os.chdir(r"../../")
        with open("result.txt", 'a+') as file:
            file.write(str(res)+"\n")

    def start_test(self, remote_desktop, remote_shell, keylogger, download_and_execute):
        try:
            res = {}
            res["rat_name"] = self.__class__.__name__
            self.server_start()
            res["pid"] = self.server.pid
            res["server_start"] = self.server_start_time
            remote_desktop_res = phf_state.no_need
            remote_shell_res = phf_state.no_need
            keylogger_res = phf_state.no_need
            download_and_execute_res = phf_state.no_need
            time.sleep(1)
            if remote_desktop:
                start_time = time.time()
                remote_desktop_res = self.remote_desktop()
                end_time = time.time()
                res["remote_desktop"] = {
                    "start":start_time,
                    "end":end_time
                }
            time.sleep(1)
            if remote_shell:
                start_time = time.time()
                remote_shell_res = self.remote_shell()
                end_time = time.time()
                res["remote_shell"] = {
                    "start": start_time,
                    "end": end_time
                }
            time.sleep(1)
            if keylogger:
                start_time = time.time()
                keylogger_res = self.key_logger()
                end_time = time.time()
                res["keylogger"] = {
                    "start": start_time,
                    "end": end_time
                }
            time.sleep(1)
            if download_and_execute:
                start_time = time.time()
                download_and_execute_res = self.download_execute()
                end_time = time.time()
                res["download_and_execute"] = {
                    "start": start_time,
                    "end": end_time
                }
            time.sleep(10)
            self.write_res(res)
            self.close_software()
            return remote_desktop_res, remote_shell_res, keylogger_res, download_and_execute_res
        except:
            logger.error(traceback.format_exc())
            self.close_software()
            return False, False, False, False