from ratClass.rat import Rat
from config import inc
from pywinauto.application import Application
from utils.phf_operation import retry_if_fail, not_implement
from utils import get_logger
import subprocess
import threading
import time
import os

logger = get_logger.get_common_logger()

class RevengeRat(Rat):
    version = "0.3"

    def __init__(self):
        logger.info("start testing {}".format(self.__class__.__name__))
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        self.server_start_time = time.time()
        self.server = subprocess.Popen(os.path.abspath(r"resources\Alibaba-Cloud-Guard-RATs\Rat\Revenge-RAT v0.3\Revenge-RAT v0.3\Client.exe"), shell=False)
        self.server_pid = self.server.pid
        logger.debug("revenge rat server pid: {}".format(self.server.pid))
        self.app = Application(backend="uia").start(os.path.abspath(r'resources\Alibaba-Cloud-Guard-RATs\Rat\Revenge-RAT v0.3\Revenge-RAT v0.3\Revenge-RAT v0.3.exe'))

    def server_start(self):
        self.dlg = self.app.window(title_re="Revenge-RAT*")
        self.dlg.child_window(auto_id="CPort").child_window(control_type="Edit").set_text(333)
        self.dlg.child_window(title="Start Listening").click()

    @retry_if_fail(inc.rats_retry_times)
    def remote_shell(self):
        self.dlg = self.app.window(auto_id="Main")
        self.dlg.set_focus()
        self.dlg.child_window(auto_id="Listview1").ListItem.wait('exists', timeout=120)
        self.dlg.child_window(auto_id="Listview1").ListItem.click_input(button='right')
        self.dlg.child_window(title="Execute").select()
        self.dlg.child_window(title="Execute").child_window(title="Scripts").click_input()
        time.sleep(1)
        self.dlg = self.app.window(auto_id="OpenScript0")
        self.dlg.child_window(title="PowerShell").click_input()
        # self.dlg.child_window(auto_id="TableLayoutPanel10").set_text("Echo hello")
        self.dlg.child_window(title="Execute in powershell").click()
        time.sleep(1)
        self.dlg.close()

    @retry_if_fail(inc.rats_retry_times)
    def key_logger(self):
        self.dlg = self.app.window(auto_id="Main")
        self.dlg.set_focus()
        self.dlg.child_window(auto_id="Listview1").ListItem.wait('exists')
        self.dlg.child_window(auto_id="Listview1").ListItem.click_input(button='right')
        self.dlg.child_window(title="Control Center").select()
        self.dlg.child_window(title="Control Center").child_window(title="Keylogger").click_input()

        self.dlg = self.app.window(title_re="Keylogger*")
        self.dlg.click_input()
        # self.dlg.print_control_identifiers()
        self.dlg.type_keys(inc.keylogger_command)
        self.dlg.Refresh.click()
        self.dlg.type_keys("aaaaaa")
        self.dlg.Refresh.click()
        time.sleep(2)
        self.dlg.close()

    # yi shi xian, dan shi hui shan tui
    @not_implement
    def download_execute(self):
        pass

    # @retry_if_fail(inc.rats_retry_times)
    # def download_execute(self):
    #     self.dlg = self.app.window(auto_id="Main")
    #     self.dlg.set_focus()
    #     self.dlg.child_window(auto_id="Listview1").ListItem.wait('exists')
    #     self.dlg.child_window(auto_id="Listview1").ListItem.click_input(button='right')
    #     self.dlg.child_window(title="Execute").select()
    #     self.dlg.child_window(title="Execute").child_window(title="Download and execute").click_input()
    #
    #     time.sleep(1)
    #     self.dlg.child_window(auto_id="URL").set_text(inc.download_and_execute_file_url)
    #     self.dlg.child_window(auto_id="FN").set_text(inc.download_and_execute_file_name)
    #     self.dlg.child_window(auto_id="DAEC").click()
    #     time.sleep(10)
    #     # self.dlg.print_control_identifiers()

    def close_software(self):
        try:
            self.app.kill()
        except:
            os.system("taskkill /F /PID {}".format(self.app.process))
        self.server.terminate()
        os.system("taskkill /F /IM Client.exe")
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))

    @not_implement
    def remote_desktop(self):
        pass

    # def start_test(self):
    #     self.server_start()
    #     self.remote_shell()
    #     self.key_logger()
    #     self.download_execute()
    #     self.close_software()

if __name__ == '__main__':
    os.chdir(os.path.abspath(os.path.join(os.getcwd(), "../resources/TA1-Binaries-master")))
    rat = RevengeRat()
    rat.start_test(False, False, False, True)
    # rat.start_test(False, False, False, False)