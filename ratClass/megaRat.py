from ratClass.rat import Rat
from config import inc
from pywinauto.application import Application
import subprocess
import threading
import time
import os

# useless
class MegaRat(Rat):
    version = "1.5 Beta"

    def __init__(self):
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        self.app = Application(backend="uia").start(r'resources\resources\Alibaba-Cloud-Guard-RATs\Rat\Mega RAT 1.5 Beta\Mega RAT Beta\Mega RAT 1.5 Beta.exe')
        self.app.top_window().print_control_identifiers()

    def server_start(self):
        self.dlg = self.app.window(title="Port Settings")
        self.dlg.print_control_identifiers()
        self.dlg.child_window(control_type="Edit").set_text("8745")
        self.dlg.Start.click()
        time.sleep(2)
        self.app.top_window().print_control_identifiers()

    def start_test(self):
        self.server_start()
        pass

if __name__ == '__main__':
    rat = MegaRat()
    # rat.start_test()