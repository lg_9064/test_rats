from ratClass.rat import Rat
from pywinauto.application import Application
from config import inc
from utils.phf_operation import retry_if_fail
from utils import get_logger
import subprocess
import threading
import time
import traceback
import os

logger = get_logger.get_common_logger()

class ImminentRat(Rat):
    version = "3.9.0.0"

    def __init__(self):
        logger.info("start testing {}".format(self.__class__.__name__))
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        self.server_start_time = time.time()
        self.server = subprocess.Popen(os.path.abspath(r"resources\Alibaba-Cloud-Guard-RATs\Rat\Imminent Monitor 3.9.0.0\IMServer.exe"), shell=False)
        self.server_pid = self.server.pid
        logger.debug("imminent rat server pid: {}".format(self.server.pid))
        try:
            self.app = Application(backend="uia").start(os.path.abspath(r'resources\Alibaba-Cloud-Guard-RATs\Rat\Imminent Monitor 3.9.0.0\Imminent Monitor 3.9.exe'))

            try:
                self.app.window(title="Welcome").child_window(auto_id="2").click()
                time.sleep(1)
                self.app.window(title="Welcome").child_window(auto_id="2").click()
            except:
                self.app.top_window().child_window(auto_id="2").click()
                time.sleep(1)
                self.app.top_window().child_window(auto_id="2").click()
            self.dlg = self.app.window(auto_id="FormMain")
            self.dlg.child_window(title="Clients").click_input()
            self.dlg.maximize()
        except:
            self.close_software()
            raise

    @retry_if_fail(inc.rats_retry_times)
    def remote_desktop(self):
        self.dlg = self.app.window(auto_id="FormMain")
        self.dlg.set_focus()
        while not self.dlg.child_window(auto_id="AeroListviewClients").ListItem.exists():
            time.sleep(0.5)
        self.dlg.set_focus()
        self.dlg.child_window(auto_id="AeroListviewClients").ListItem.click_input(button='right')
        self.dlg.child_window(title="DropDown").child_window(title="Surveillance").select()
        self.dlg.child_window(title="DropDown").child_window(title="Surveillance").child_window(title="Remote Desktop").select()
        time.sleep(1)
        self.dlg = self.app.window(auto_id="FormDesktop")
        self.dlg.MenuItem0.click_input()
        time.sleep(inc.remote_desktop_sleep_time)
        self.dlg.MenuItem2.click_input()
        time.sleep(1)
        self.dlg.close()

    @retry_if_fail(inc.rats_retry_times)
    def remote_shell(self):
        self.dlg = self.app.window(auto_id="FormMain")
        self.dlg.set_focus()
        while not self.dlg.child_window(auto_id="AeroListviewClients").ListItem.exists():
            time.sleep(0.5)
        self.dlg.set_focus()
        self.dlg.child_window(auto_id="AeroListviewClients").ListItem.click_input(button='right')
        self.dlg.child_window(title="DropDown").child_window(title="Scripting").select()
        time.sleep(1)
        self.dlg = self.app.window(auto_id="ScriptingForm")
        self.dlg.child_window(title="Batch").click_input()
        self.dlg.child_window(control_type="Edit").set_text(inc.remote_shell_command[0])
        self.dlg.RunScript.click()
        self.dlg.close()

    @retry_if_fail(inc.rats_retry_times)
    def key_logger(self):
        self.dlg = self.app.window(auto_id="FormMain")
        self.dlg.set_focus()
        while not self.dlg.child_window(auto_id="AeroListviewClients").ListItem.exists():
            time.sleep(0.5)
        self.dlg.set_focus()
        self.dlg.child_window(auto_id="AeroListviewClients").ListItem.click_input(button='right')
        self.dlg.child_window(title="DropDown").child_window(title="Surveillance").select()
        self.dlg.child_window(title="DropDown").child_window(title="Surveillance").child_window(title="Keylogger").select()
        time.sleep(1)
        self.dlg = self.app.window(auto_id="FormKeylogger")
        self.dlg.child_window(title="Live Keylogger").click_input()
        self.dlg.Start.click()
        self.dlg.child_window(control_type="Edit").type_keys(inc.keylogger_command)
        self.dlg.Stop.click()
        time.sleep(1)
        self.dlg.close()
        # self.app.top_window().print_control_identifiers()

    @retry_if_fail(inc.rats_retry_times)
    def download_execute(self):
        self.dlg = self.app.window(auto_id="FormMain")
        while not self.dlg.child_window(auto_id="AeroListviewClients").ListItem.exists():
            time.sleep(0.5)
        self.dlg.set_focus()
        self.dlg.child_window(auto_id="AeroListviewClients").ListItem.click_input(button='right')

        for i in range(5):
            self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        for i in range(1):
            self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        for i in range(1):
            self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        time.sleep(1)

        self.dlg = self.app.window(auto_id="FormExecuteUpdate")
        self.dlg.child_window(auto_id="TextBoxURL").set_text(inc.download_and_execute_file_url)
        self.dlg.Download.click()
        time.sleep(5)
        self.dlg.close()
        # self.dlg = self.app.window(auto_id="FormMain")
        # while not self.dlg.child_window(auto_id="AeroListviewClients").ListItem.exists():
        #     time.sleep(0.5)
        # self.dlg.child_window(auto_id="AeroListviewClients").ListItem.click_input(button='right')
        # self.dlg.child_window(title="DropDown").child_window(title="Client").select()
        # time.sleep(1)
        # self.dlg.child_window(title="DropDown").child_window(title="Client").child_window(title="Remote Execute").click_input()
        # time.sleep(1)
        # self.dlg.child_window(title="DropDown").child_window(title="Client").child_window(title="Remote Execute").child_window(title="From Link").click_input()

    def close_software(self):
        try:
            self.app.kill()
        except:
            os.system("taskkill /F /PID {}".format(self.app.process))
        self.server.terminate()
        os.system("taskkill /F /IM IMServer.exe")
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))


    # def start_test(self):
    #     self.remote_desktop()
    #     self.remote_shell()
    #     self.key_logger()
    #     self.download_execute()
    #     self.close_software()

if __name__ == '__main__':
    os.chdir(os.path.abspath(os.path.join(os.getcwd(), "../resources/TA1-Binaries-master")))
    rat = ImminentRat()
    rat.start_test(True, False, False, False)
