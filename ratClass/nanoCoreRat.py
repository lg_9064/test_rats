from ratClass.rat import Rat
from config import inc
from pywinauto.application import Application
import subprocess
import threading
import time
import os

# useless
class NanoCoreRat(Rat):
    version = "1.2.2.0"

    def __init__(self):
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        self.app = Application(backend="uia").start(r'..\resources\Alibaba-Cloud-Guard-RATs\Rat\NanoCore 1.2.2.0\NanoCore.exe')
        self.app.window(auto_id="ServerMainForm").wait('exists')
        self.dlg = self.app.window(auto_id="ServerMainForm")
        self.dlg.print_control_identifiers()
        self.dlg.child_window(auto_id="NavigationControl1").print_control_identifiers()
        self.dlg.child_window(auto_id="NavigationControl1").click_input()


    def start_test(self):
        pass

if __name__ == '__main__':
    rat = NanoCoreRat()
    rat.start_test()