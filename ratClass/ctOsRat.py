from ratClass.rat import Rat
from pywinauto.application import Application
from config import inc
from utils.phf_operation import retry_if_fail, not_implement
from utils import get_logger
import subprocess
import threading
import time
import traceback
import os

logger =get_logger.get_common_logger()

class CtOsRat(Rat):
    version = "0.1"


    def __init__(self):
        logger.info("start testing {}".format(self.__class__.__name__))
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        self.server_start_time = time.time()
        self.server = subprocess.Popen(os.path.abspath(r"resources\Alibaba-Cloud-Guard-RATs\Rat\ctOs 1.3.0.0\server.exe"), shell=False)
        self.server_pid = self.server.pid
        logger.debug("ctOs rat server pid: {}".format(self.server.pid))
        self.app = Application(backend="uia").start(os.path.abspath(r'resources\Alibaba-Cloud-Guard-RATs\Rat\ctOs 1.3.0.0\ctOs_Cracked By Alcatraz3222.exe'))


    def server_start(self):
        self.dlg = self.app.window(auto_id="Form1")
        self.dlg.set_focus()
        self.dlg.child_window(auto_id="ThirteenForm1").Start.click()
        time.sleep(1)

    @retry_if_fail(inc.rats_retry_times)
    def remote_desktop(self):
        self.dlg = self.app.window(auto_id="Form1")
        self.dlg.set_focus()
        self.dlg.child_window(auto_id="ThirteenForm1").child_window(auto_id="ThirteenTabControl1").child_window(auto_id="TabPage2").child_window(auto_id="ListView1").ListItem.click_input(button='right')
        time.sleep(1)
        self.dlg.child_window(title="DropDown").child_window(title="Surveillance").select()
        self.dlg.child_window(title="DropDown").child_window(title="Surveillance").child_window(title="Remote Desktop").select()
        self.dlg = self.app.window(auto_id="RemoteDesktop")
        self.dlg.child_window(title="Remote Desktop").Start.click()
        time.sleep(inc.remote_desktop_sleep_time)
        self.dlg.child_window(title="Remote Desktop").Stop.click()
        time.sleep(1)
        self.dlg.close()
        # self.app.top_window().print_control_identifiers()

    @not_implement
    def remote_shell(self):
        pass

    @not_implement
    def download_execute(self):
        pass

    @retry_if_fail(inc.rats_retry_times)
    def key_logger(self):
        self.dlg = self.app.window(auto_id="Form1")
        self.dlg.set_focus()
        self.dlg.child_window(auto_id="ThirteenForm1").child_window(auto_id="ThirteenTabControl1").child_window(
            auto_id="TabPage2").child_window(auto_id="ListView1").ListItem.click_input(button='right')
        time.sleep(1)
        self.dlg.child_window(title="DropDown").child_window(title="Surveillance").select()
        self.dlg.child_window(title="DropDown").child_window(title="Surveillance").child_window(
            title="Keylogger").select()
        time.sleep(1)
        self.dlg = self.app.window(auto_id="Keyloggher")
        self.dlg.child_window(auto_id="ThirteenForm1").Start.click()
        self.dlg.type_keys(inc.keylogger_command)
        time.sleep(1)
        self.dlg.child_window(auto_id="ThirteenForm1").Stop.click()
        time.sleep(1)
        self.dlg.close()

    def close_software(self):
        try:
            self.app.kill()
        except:
            os.system("taskkill /F /PID {}".format(self.app.process))
        self.server.terminate()
        os.system("taskkill /F /IM server.exe")
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))

    # def start_test(self):
    #     self.server_start()
    #     self.remote_desktop()
    #     self.key_logger()
    #     self.close_software()

if __name__ == '__main__':
    os.chdir(os.path.abspath(os.path.join(os.getcwd(), "../resources/TA1-Binaries-master")))
    rat = CtOsRat()
    rat.start_test(True, True, True, True)