from ratClass.rat import Rat
from pywinauto.application import Application
from config import inc
from utils.phf_operation import retry_if_fail, not_implement
from utils import get_logger
import subprocess
import threading
import time
import traceback
import os


logger = get_logger.get_common_logger()

class ImperiumRat(Rat):
    version = ""

    def __init__(self):
        logger.info("start testing {}".format(self.__class__.__name__))
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        self.server_start_time = time.time()
        self.server = subprocess.Popen(os.path.abspath(r"resources\Alibaba-Cloud-Guard-RATs\Rat\Imperium RAT Cracked\Imperium RAT Cracked\server.exe"), shell=False)
        logger.debug("imperium rat server pid: {}".format(self.server.pid))
        self.app = Application(backend="uia").start(os.path.abspath(r'resources\Alibaba-Cloud-Guard-RATs\Rat\Imperium RAT Cracked\Imperium RAT Cracked\imperium.exe'))
        self.server_pid = self.server.pid
        self.dlg = self.app.window(auto_id="Form1")

    @retry_if_fail(inc.rats_retry_times)
    def server_start(self):
        self.dlg.set_focus()
        self.dlg.Settings.click_input()
        self.dlg.child_window(auto_id="Button12").click()
        self.dlg.Connections.click_input()

    @retry_if_fail(inc.rats_retry_times)
    def remote_desktop(self):
        self.dlg = self.app.window(auto_id="Form1")
        self.dlg.set_focus()
        while not self.dlg.child_window(auto_id="ListView1").ListItem.exists():
            time.sleep(0.5)
        self.dlg.child_window(auto_id="ListView1").ListItem.click_input(button='right')
        # .....
        for i in range(5):
            self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        for i in range(2):
            self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        self.dlg.type_keys('{ENTER}')

        time.sleep(1)
        self.dlg = self.app.window(auto_id="ImageBox")
        time.sleep(inc.remote_desktop_sleep_time)
        self.dlg.close()
        # self.dlg.child_window(title="Managers").click_input()
        # self.dlg.child_window(title="Screenshot Manager").click_input()
        # # self.dlg.print_control_identifiers()
        # # # self.app.top_window().print_control_identifiers()
        # # self.dlg = self.app.top_window().child_window(title="Start").click_input()
        # self.dlg.print_control_identifiers()
        # self.dlg.child_window(title="Start").click_input()

    @retry_if_fail(inc.rats_retry_times)
    def remote_shell(self):
        self.dlg = self.app.window(auto_id="Form1")
        self.dlg.set_focus()
        while not self.dlg.child_window(auto_id="ListView1").ListItem.exists():
            time.sleep(0.5)
        self.dlg.child_window(auto_id="ListView1").ListItem.click_input(button='right')
        # .....
        for i in range(4):
            self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        for i in range(4):
            self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        for i in range(1):
            self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')

        time.sleep(1)
        self.dlg = self.app.window(auto_id="Form1")
        self.dlg.child_window(control_type="Edit").set_text(inc.remote_shell_command[0])
        self.dlg.OK.click()


    @retry_if_fail(inc.rats_retry_times)
    def key_logger(self):
        self.dlg = self.app.window(auto_id="Form1")
        self.dlg.set_focus()
        while not self.dlg.child_window(auto_id="ListView1").ListItem.exists():
            time.sleep(0.5)
        self.dlg.child_window(auto_id="ListView1").ListItem.click_input(button='right')
        for i in range(5):
            self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        self.dlg.type_keys('{ENTER}')
        self.dlg.type_keys('{ENTER}')
        time.sleep(1)
        self.dlg = self.app.window(auto_id="KBox")
        self.dlg.child_window(control_type="Edit").type_keys(inc.keylogger_command)
        time.sleep(1)
        self.dlg.close()

    # shan tui
    @not_implement
    def download_execute(self):
        pass

    # @retry_if_fail(inc.rats_retry_times)
    # def download_execute(self):
    #     self.dlg = self.app.window(auto_id="Form1")
    #     self.dlg.set_focus()
    #     while not self.dlg.child_window(auto_id="ListView1").ListItem.exists():
    #         time.sleep(0.5)
    #     self.dlg.child_window(auto_id="ListView1").ListItem.click_input(button='right')
    #     for i in range(6):
    #         self.dlg.type_keys('{DOWN}')
    #     self.dlg.type_keys('{ENTER}')
    #     for i in range(3):
    #         self.dlg.type_keys('{DOWN}')
    #     self.dlg.type_keys('{ENTER}')
    #
    #     time.sleep(1)
    #     try:
    #         self.dlg.child_window(control_type="Edit").set_text(inc.download_and_execute_file_url)
    #         self.dlg.OK.click()
    #     except:
    #         self.dlg = self.app.top_window()
    #         self.dlg.child_window(control_type="Edit").set_text(inc.download_and_execute_file_url)
    #         self.dlg.OK.click()
    #     time.sleep(2)

    def close_software(self):
        try:
            self.app.kill()
        except:
            os.system("taskkill /F /PID {}".format(self.app.process))
        self.server.terminate()
        os.system("taskkill /F /IM server.exe")
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))



    # def start_test(self):
    #     self.server_start()
    #     self.remote_desktop()
    #     self.remote_shell()
    #     self.key_logger()
    #     self.download_execute()
    #     self.close_software()


if __name__ == '__main__':
    os.chdir(os.path.abspath(os.path.join(os.getcwd(), "../resources/TA1-Binaries-master")))
    rat = ImperiumRat()
    # rat.start_test(True, True, True, True)
    rat.start_test(False, False, False, True)