from ratClass.rat import Rat
from pywinauto.application import Application
from config import inc
from utils.phf_operation import retry_if_fail, not_implement
from utils import get_logger
import subprocess
import threading
import time
import os

logger = get_logger.get_common_logger()

class GreameRat(Rat):
    version = "1.9"


    def __init__(self):
        logger.info("start testing {}".format(self.__class__.__name__))
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        self.server_start_time = time.time()
        self.server = subprocess.Popen(os.path.abspath(r"resources\Alibaba-Cloud-Guard-RATs\Rat\Greame RAT v1.9\Greame RAT v1.9\server.exe"), shell=False)
        self.server_pid = self.server.pid
        logger.debug("greame rat server pid: {}".format(self.server.pid))
        self.app = Application(backend="uia").start(os.path.abspath(r'resources\Alibaba-Cloud-Guard-RATs\Rat\Greame RAT v1.9\Greame RAT v1.9\GreameRAT.exe'))
        self.dlg = self.app.window(control_type="Window")
        self.dlg.maximize()
        self.dlg.set_focus()
        # self.dlg = self.app.window(auto_id="Main")

    @retry_if_fail(inc.rats_retry_times)
    def remote_desktop(self):
        time.sleep(1)
        self.dlg = self.app.window(control_type="Window")
        self.dlg.set_focus()
        while not self.dlg.child_window(title="pnl1").child_window(class_name="TListView").ListItem.exists():
            time.sleep(1)
        self.dlg.child_window(title="pnl1").child_window(class_name="TListView").ListItem.click_input(
            button="left")
        time.sleep(2)
        self.dlg.child_window(title="pnl1").child_window(class_name="TListView").ListItem.click_input(
            button="right")
        self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        self.dlg.type_keys('{ENTER}')
        # self.app.top_window().MenuItem.click_input()
        # time.sleep(1)
        # self.app.top_window().MenuItem.select()
        time.sleep(inc.remote_desktop_sleep_time)
        self.dlg = self.app.window(title_re="Server*")
        time.sleep(5)
        self.dlg.close()
        # self.dlg.child_window(title="Save").exists()
        # self.app.top_window().print_control_identifiers()
        # self.app.window(best_match="Remote Desktop").close()
        # self.dlg.child_window(title="Country").parent().parent().ListItem.click_input(button="right")

    @retry_if_fail(inc.rats_retry_times)
    def remote_shell(self):
        time.sleep(1)
        self.dlg = self.app.window(control_type="Window")
        self.dlg.set_focus()
        while not self.dlg.child_window(title="pnl1").child_window(class_name="TListView").ListItem.exists():
            time.sleep(1)
        self.dlg.child_window(title="pnl1").child_window(class_name="TListView").ListItem.click_input(
            button="left")
        self.dlg.child_window(title="pnl1").child_window(class_name="TListView").ListItem.click_input(
        button="right")
        for i in range(7):
            self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        time.sleep(1)
        self.dlg = self.app.window(class_name="TForm")
        self.dlg.child_window(control_type="Edit").set_text(inc.remote_shell_command[0])
        self.dlg.OK.click()

    @retry_if_fail(inc.rats_retry_times)
    def key_logger(self):
        time.sleep(1)
        self.dlg = self.app.window(control_type="Window")
        self.dlg.set_focus()
        while not self.dlg.child_window(title="pnl1").child_window(class_name="TListView").ListItem.exists():
            time.sleep(1)
        self.dlg.child_window(title="pnl1").child_window(class_name="TListView").ListItem.click_input(
            button="left")
        self.dlg.child_window(title="pnl1").child_window(class_name="TListView").ListItem.click_input(
        button="right")
        self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        for i in range(4):
            self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        # self.app.top_window().MenuItem.click_input()
        # time.sleep(1)
        # self.app.top_window().menuItem5.select()
        time.sleep(2)
        self.dlg = self.app.window(title_re="Server*")
        self.dlg.child_window(class_name="TMemo").type_keys(inc.keylogger_command)
        time.sleep(1)
        self.dlg.close()

    @not_implement
    def download_execute(self):
        pass
    #     # while not self.dlg.child_window(title="pnl1").child_window(class_name="TListView").ListItem.exists():
    #     #     time.sleep(1)
    #     # self.dlg.child_window(title="pnl1").child_window(class_name="TListView").ListItem.click_input(
    #     #     button="left")
    #     # self.dlg.child_window(title="pnl1").child_window(class_name="TListView").ListItem.click_input(
    #     # button="right")
    #     # self.app.top_window().MenuItem6.click_input()
    #     # self.app.top_window().menuItem.select()
    #     # time.sleep(1)
    #     # self.app.window(title="Greame RAT v1.9").print_control_identifiers()


    def close_software(self):
        time.sleep(2)
        try:
            self.app.kill()
        except:
            os.system("taskkill /F /PID {}".format(self.app.process))
        self.server.terminate()
        os.system("taskkill /F /IM server.exe")
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))


    # def start_test(self):
    #     self.remote_desktop()
    #     self.remote_shell()
    #     self.key_logger()
    #     self.close_software()
    #     # self.download_execute()


if __name__ == '__main__':
    os.chdir(os.path.abspath(os.path.join(os.getcwd(), "../resources/TA1-Binaries-master")))
    rat = GreameRat()
    rat.start_test(True, True, True, True)