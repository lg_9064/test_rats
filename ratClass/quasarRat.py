from ratClass.rat import Rat
from config import inc
from pywinauto.application import Application
from utils.phf_operation import retry_if_fail, not_implement
from utils import get_logger
import subprocess
import threading
import time
import os
import traceback

logger = get_logger.get_common_logger()

class QuasarRat(Rat):
    version = "1.3.0.0"

    def __init__(self):
        logger.info("start testing {}".format(self.__class__.__name__))
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        self.server_start_time = time.time()
        self.server = subprocess.Popen(os.path.abspath(r"resources\Alibaba-Cloud-Guard-RATs\Rat\Quasar.v1.3.0.0\Quasar v1.3.0.0\Client-built.exe"), shell=False)
        self.server_pid = self.server.pid
        logger.debug("quasar rat server pid: {}".format(self.server.pid))
        self.app = Application(backend="uia").start(os.path.abspath(r'resources\Alibaba-Cloud-Guard-RATs\Rat\Quasar.v1.3.0.0\Quasar v1.3.0.0\Quasar.exe'))
        self.dlg = self.app.window(auto_id="FrmMain")

    def server_start(self):
        self.dlg = self.app.window(auto_id="FrmMain")
        self.dlg.set_focus()
        self.dlg.child_window(title="Settings").click_input()
        time.sleep(1)
        try:
            self.dlg.child_window(title="Start listening").click()
            time.sleep(1)
            self.dlg.child_window(title="Save").click()
        except:
            traceback.format_exc()
            self.dlg = self.app.top_window()
            self.dlg.child_window(title="Start listening").click()
            time.sleep(1)
            self.dlg.child_window(title="Save").click()

    @retry_if_fail(inc.rats_retry_times)
    def remote_desktop(self):
        self.dlg = self.app.window(auto_id="FrmMain")
        self.dlg.set_focus()
        self.dlg.child_window(auto_id='lstClients').child_window(control_type="ListItem").wait("exists", timeout=60)
        self.dlg.child_window(auto_id='lstClients').child_window(control_type="ListItem").click_input(button='right')
        time.sleep(1)
        self.dlg.child_window(title="DropDown").child_window(title="Surveillance").select()
        self.dlg.child_window(title="DropDown").child_window(title="Surveillance").child_window(title="Remote Desktop").click_input()
        time.sleep(1)
        self.dlg = self.app.window(auto_id="FrmRemoteDesktop")
        self.dlg.Start.click()
        time.sleep(inc.remote_desktop_sleep_time)
        self.dlg.Stop.click()
        self.dlg.close()

    @retry_if_fail(inc.rats_retry_times)
    def remote_shell(self):
        self.dlg = self.app.window(auto_id="FrmMain")
        self.dlg.set_focus()
        self.dlg.child_window(auto_id='lstClients').child_window(control_type="ListItem").wait("exists", timeout=60)
        self.dlg.child_window(auto_id='lstClients').child_window(control_type="ListItem").click_input(button='right')
        time.sleep(1)
        self.dlg.child_window(title="DropDown").child_window(title="System").select()
        self.dlg.child_window(title="DropDown").child_window(title="System").child_window(title="Remote Shell").click_input()
        time.sleep(1)
        self.dlg = self.app.window(auto_id="FrmRemoteShell")
        for cmd in inc.remote_shell_command:
            self.dlg.child_window(auto_id="txtConsoleInput").set_text(cmd)
            self.dlg.type_keys('{ENTER}')
        time.sleep(2)
        self.dlg.close()


    @not_implement
    def key_logger(self):
        pass
    #     # self.dlg.child_window(auto_id='lstClients').child_window(control_type="ListItem").wait("exists", timeout=60)
    #     # self.dlg.child_window(auto_id='lstClients').child_window(control_type="ListItem").click_input(button='right')
    #     # time.sleep(1)
    #     # self.dlg.child_window(title="DropDown").child_window(title="Surveillance").select()
    #     # self.dlg.child_window(title="DropDown").child_window(title="Surveillance").child_window(title="Keylogger").click_input()
    #     # time.sleep(1)
    #     # self.dlg = self.app.window(auto_id="FrmKeylogger")
    #     # self.dlg.print_control_identifiers()
    #     pass

    @retry_if_fail(inc.rats_retry_times)
    def download_execute(self):
        self.dlg = self.app.window(auto_id="FrmMain")
        self.dlg.child_window(auto_id='lstClients').child_window(control_type="ListItem").wait("exists", timeout=60)
        self.dlg.child_window(auto_id='lstClients').child_window(control_type="ListItem").click_input(button='right')
        time.sleep(1)
        for i in range(4):
            self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        self.dlg.type_keys('{ENTER}')
        self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        time.sleep(1)
        try:
            self.dlg.child_window(auto_id="txtURL").set_text(inc.download_and_execute_file_url)
            self.dlg.child_window(auto_id="btnDownloadAndExecute").click()
        except:
            traceback.format_exc()
            self.dlg = self.app.top_window()
            self.dlg.child_window(auto_id="txtURL").set_text(inc.download_and_execute_file_url)
            self.dlg.child_window(auto_id="btnDownloadAndExecute").click()
        time.sleep(1)


    def close_software(self):
        try:
            self.app.kill()
        except:
            os.system("taskkill /F /PID {}".format(self.app.process))
        self.server.terminate()
        os.system("taskkill /F /IM Client-built.exe")
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))

    # def start_test(self):
    #     self.server_start()
    #     self.remote_desktop()
    #     self.remote_shell()
    #     self.download_execute()
    #     self.close_software()

if __name__ == '__main__':
    os.chdir(os.path.abspath(os.path.join(os.getcwd(), "../resources/TA1-Binaries-master")))
    rat = QuasarRat()
    rat.start_test(True, True, True, True)