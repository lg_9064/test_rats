from ratClass.rat import Rat
from pywinauto.application import Application
from config import inc
from utils.phf_operation import retry_if_fail, not_implement
from utils import get_logger
import subprocess
import threading
import time
import os

logger = get_logger.get_common_logger()

class L6Rat(Rat):
    version = "BETA 1"

    def __init__(self):
        logger.info("start testing {}".format(self.__class__.__name__))
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        self.server_start_time = time.time()
        self.server = subprocess.Popen(os.path.abspath(r"resources\Alibaba-Cloud-Guard-RATs\Rat\L6-RAT Beta 1\L6-RAT Beta 1\Server.exe"), shell=False)
        self.server_pid = self.server.pid
        logger.debug("l6 rat server pid: {}".format(self.server.pid))
        self.app = Application(backend="uia").start(os.path.abspath(r'resources\Alibaba-Cloud-Guard-RATs\Rat\L6-RAT Beta 1\L6-RAT Beta 1\L6-Rat Beta 1.exe'))
        self.app_pid = self.app.process
        self.dlg = self.app.window(auto_id="VBInputBox")
        self.dlg.child_window(auto_id="OKButton").click()
        self.dlg = self.app.window(auto_id="Form1")
    #
    @not_implement
    def remote_desktop(self):
        pass

    @retry_if_fail(inc.rats_retry_times)
    def remote_shell(self):
        self.dlg = self.app.window(auto_id="Form1")
        self.dlg.set_focus()
        while not self.dlg.child_window(auto_id="L1").ListItem.exists():
            time.sleep(0.5)
        while not self.app.top_window().child_window(auto_id="T2").exists():
            self.dlg.child_window(auto_id="L1").ListItem.click_input(button='right')
            self.dlg.child_window(title="DropDown").child_window(title="Remote Shell").select()
            time.sleep(1)

        self.dlg = self.app.top_window()
        for cmd in inc.remote_shell_command:
            self.dlg.child_window(auto_id="T2").set_text(cmd)
            self.dlg.type_keys('{ENTER}')
        time.sleep(2)
        self.dlg.close()
        # self.app.top_window().print_control_identifiers()
    @retry_if_fail(inc.rats_retry_times)
    def key_logger(self):
        self.dlg = self.app.window(auto_id="Form1")
        self.dlg.set_focus()
        while not self.dlg.child_window(auto_id="L1").ListItem.exists():
            time.sleep(0.5)
        while not self.app.top_window().child_window(auto_id="T1").exists():
            self.dlg.child_window(auto_id="L1").ListItem.click_input(button='right')
            self.dlg.child_window(title="DropDown").child_window(title="Keylogger").select()
            time.sleep(1)
        self.dlg = self.app.top_window()
        self.dlg.child_window(control_type="Edit").type_keys(inc.keylogger_command)
        time.sleep(1)
        self.dlg.close()

    @retry_if_fail(inc.rats_retry_times)
    def download_execute(self):
        self.dlg = self.app.window(auto_id="Form1")
        self.dlg.set_focus()
        while not self.dlg.child_window(auto_id="L1").ListItem.exists():
            time.sleep(0.5)
        while not self.app.top_window().child_window(auto_id="FURL").exists():
            self.dlg.child_window(auto_id="L1").ListItem.click_input(button='right')
            self.dlg.child_window(title="DropDown").child_window(title="Run File").select()
            self.dlg.child_window(title="DropDown").child_window(title="Run File").child_window(title="From Link").click_input()
            time.sleep(1)
        self.dlg.child_window(auto_id="TextBox1").set_text(inc.download_and_execute_file_url)
        self.dlg.Run.click()
        time.sleep(10)

    def close_software(self):
        try:
            self.app.kill()
        except:
            os.system("taskkill /F /PID {}".format(self.app.process))
        self.server.terminate()
        os.system("taskkill /F /IM Server.exe")
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))

    # def start_test(self):
    #     self.remote_shell()
    #     self.key_logger()
    #     self.download_execute()
    #     self.close_software()

if __name__ == '__main__':
    os.chdir(os.path.abspath(os.path.join(os.getcwd(), "../resources/TA1-Binaries-master")))
    rat = L6Rat()
    rat.start_test(True, True, True, True)