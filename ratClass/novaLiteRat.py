from ratClass.rat import Rat
from config import inc
from pywinauto.application import Application
from utils.phf_operation import retry_if_fail, not_implement
from utils import get_logger
import subprocess
import threading
import time
import os

logger = get_logger.get_common_logger()

class NovaLiteRat(Rat):
    version = "3.0"

    def __init__(self):
        logger.info("start testing {}".format(self.__class__.__name__))
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        self.server_start_time = time.time()
        self.server = subprocess.Popen(os.path.abspath(r"resources\Alibaba-Cloud-Guard-RATs\Rat\NovaLite v3.0\Server.exe"), shell=False)
        self.server_pid = self.server.pid
        logger.debug("novaLite rat server pid: {}".format(self.server.pid))
        self.app = Application(backend="uia").start(os.path.abspath(r'resources\Alibaba-Cloud-Guard-RATs\Rat\NovaLite v3.0\NovaLite.exe'))
        self.dlg = self.app.window(title="NovaLite v3.0")
        self.dlg.child_window(control_type="List").child_window(control_type="CheckBox").wait('exists', timeout=60)
        # self.dlg.print_control_identifiers()

    @retry_if_fail(inc.rats_retry_times)
    def remote_desktop(self):
        self.dlg = self.app.window(title="NovaLite v3.0")
        self.dlg.set_focus()
        self.dlg.type_keys('{DOWN}')
        self.dlg.child_window(control_type="List").child_window(control_type="CheckBox").click_input(button='right')
        time.sleep(1)
        self.dlg = self.app.top_window()
        self.dlg.child_window(title="Screen Capture").click_input()
        time.sleep(1)
        self.dlg = self.app.window(title_re="NovaLite - Screen*")
        # self.dlg = self.app.top_window()
        self.dlg.child_window(title="Cap Screen").click()
        time.sleep(inc.remote_desktop_sleep_time)
        self.dlg.child_window(title="Cap Screen").click()
        time.sleep(inc.remote_desktop_sleep_time)
        self.dlg.close()
    @not_implement
    def remote_shell(self):
        pass

    @not_implement
    def key_logger(self):
        pass

    @not_implement
    def download_execute(self):
        pass

    def close_software(self):
        try:
            self.app.kill()
        except:
            os.system("taskkill /F /PID {}".format(self.app.process))
        self.server.terminate()
        os.system("taskkill /F /IM Server.exe")
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))

    # def start_test(self):
    #     self.remote_desktop()
    #     self.close_software()

if __name__ == '__main__':
    os.chdir(os.path.abspath(os.path.join(os.getcwd(), "../resources/TA1-Binaries-master")))
    rat = NovaLiteRat()
    rat.start_test(True, True, True, True)