from ratClass.rat import Rat
from config import inc
from pywinauto.application import Application
from utils.phf_operation import retry_if_fail, not_implement
from utils import get_logger
import subprocess
import threading
import time
import os

logger = get_logger.get_common_logger()

class NingaliNetRat(Rat):
    version = "1.1.0.0"

    def __init__(self):
        logger.info("start testing {}".format(self.__class__.__name__))
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        self.server_start_time = time.time()
        self.server = subprocess.Popen(os.path.abspath(r"resources\Alibaba-Cloud-Guard-RATs\Rat\NingaliNET v1.1.0.0+Source\NingaliNET v1.1.0.0\Server.exe"), shell=False)
        self.server_pid = self.server.pid
        logger.debug("ningaliNet rat server pid: {}".format(self.server.pid))
        self.app = Application(backend="uia").start(os.path.abspath(r'resources\Alibaba-Cloud-Guard-RATs\Rat\NingaliNET v1.1.0.0+Source\NingaliNET v1.1.0.0\NingaliNET v1.1.0.0-Cracked.exe'))
        self.app.window(auto_id="Form1").child_window(title="Add Listen Port").child_window(title="Start").wait('visible', timeout=60)
        self.dlg = self.app.window(auto_id='Form1')
        time.sleep(1)

    def server_start(self):
        self.dlg.set_focus()
        self.dlg.child_window(title="Add Listen Port").child_window(title="Start").click()
        self.dlg.child_window(title="User Online", control_type="Button").click()

    @retry_if_fail(inc.rats_retry_times)
    def remote_desktop(self):
        self.dlg = self.app.window(auto_id="Form1")
        self.dlg.set_focus()
        self.dlg.child_window(auto_id="L1").ListItem.wait('exists', timeout=120)
        self.dlg.child_window(auto_id="L1").ListItem.click_input(button='right')
        self.dlg.child_window(title="Control Center").select()
        self.dlg.child_window(title="Control Center").child_window(title="Remote Desktop").select()
        # self.dlg.type_keys('{DOWN}')
        # self.dlg.type_keys('{ENTER}')
        # self.dlg.type_keys('{DOWN}')
        # self.dlg.type_keys('{ENTER}')
        time.sleep(inc.remote_desktop_sleep_time)
        self.app.top_window().close()

    @retry_if_fail(inc.rats_retry_times)
    def remote_shell(self):
        self.dlg = self.app.window(auto_id="Form1")
        self.dlg.set_focus()
        self.dlg.child_window(auto_id="L1").ListItem.click_input(button='right')
        self.dlg.child_window(title="Control Center").select()
        self.dlg.child_window(title="Control Center").child_window(title="Remote Shell").select()
        time.sleep(1)
        self.dlg = self.app.window(title_re="Remote Shell*", control_type="Window")
        for cmd in inc.remote_shell_command:
            self.dlg.child_window(auto_id="T2").set_text(cmd)
            self.dlg.type_keys('{ENTER}')
        time.sleep(1)
        self.dlg.close()

    @not_implement
    def download_execute(self):
        pass

    @not_implement
    def key_logger(self):
        pass
    #     # self.dlg = self.app.window(auto_id="Form1")
    #     # self.dlg.child_window(auto_id="L1").ListItem.click_input(button='right')
    #     # self.dlg.child_window(title="Send File and Run").click_input()
    #     # time.sleep(1)
    #     # # "C:\Users\Administrator\PycharmProjects\test2\resources\softwares\cpu-z_1.86-cn.exe"
    #     # file_path = os.path.abspath(os.path.join(os.getcwd(),r"..\resources\softwares\cpu-z_1.86-cn.exe"))
    #     # self.dlg.child_window(auto_id="SearchEditBox").set_text(file_path)
    #     # time.sleep(2)
    #     # self.dlg.child_window(auto_id="0", control_type="ListItem").click_input()
    #     # self.dlg.child_window(auto_id="1",control_type="SplitButton").click_input()
    #     pass

    def close_software(self):
        try:
            self.app.kill()
        except:
            os.system("taskkill /F /IM \"NingaliNET v1.1.0.0-Cracked.exe\"")
        self.server.terminate()
        os.system("taskkill /F /IM crsd.exe")
        os.remove(os.path.join(os.environ ['USERPROFILE'], r"AppData\Local\Temp\crsd.exe"))
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))

    # def start_test(self):
    #     self.server_start()
    #     self.remote_desktop()
    #     self.remote_shell()
    #     # self.download_execute()
    #     self.close_software()
    #     # pass

if __name__ == '__main__':
    os.chdir(os.path.abspath(os.path.join(os.getcwd(), "../resources/TA1-Binaries-master")))
    rat = NingaliNetRat()
    rat.start_test(True, True, True, True)