from ratClass.rat import Rat
from pywinauto.application import Application
from config import inc
from utils.phf_operation import retry_if_fail
from utils import get_logger
import subprocess
import threading
import time
import traceback
import os

logger = get_logger.get_common_logger()

class CoringaRat(Rat):
    version = "0.1"


    def __init__(self):
        logger.info("start testing {}".format(self.__class__.__name__))
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        self.server_start_time = time.time()
        self.server = subprocess.Popen(os.path.abspath(r"resources\Alibaba-Cloud-Guard-RATs\Rat\Coringa-RAT v0.1 By SOOFT T\Coringa-RAT 0.1\Servidor.exe"), shell=False)
        self.server_pid = self.server.pid
        logger.debug("coringa rat server pid: {}".format(self.server.pid))
        time.sleep(5)
        self.app = Application(backend="uia").start(os.path.abspath(r'resources\Alibaba-Cloud-Guard-RATs\Rat\Coringa-RAT v0.1 By SOOFT T\Coringa-RAT 0.1\Coringa-RAT 0.1.exe'))

    @retry_if_fail(inc.rats_retry_times)
    def remote_desktop(self):
        self.dlg = self.app.window(auto_id="FrmMain")
        self.dlg.set_focus()
        self.dlg.child_window(control_type="ListItem", title_re="Coringa*").wait('exists', timeout=120)
        self.dlg.child_window(control_type="ListItem", best_match="Coringa").click_input(button='right')
        self.dlg.child_window(title="DropDown").MenuItem4.select()
        time.sleep(1)
        self.dlg = self.app.top_window()
        self.dlg.child_window(auto_id="Panel1").Start.click()
        time.sleep(inc.remote_desktop_sleep_time)
        self.dlg.child_window(auto_id="Panel1").Stop.click()
        self.dlg.close()

    @retry_if_fail(inc.rats_retry_times)
    def remote_shell(self):
        time.sleep(1)
        self.dlg = self.app.window(auto_id="FrmMain")
        self.dlg.set_focus()
        self.dlg.child_window(control_type="ListItem", title_re="Coringa*").wait('exists', timeout=120)

        time.sleep(2)
        # self.dlg.print_control_identifiers()
        # self.app.window()
        while not self.app.window(title_re="Coringa_CCD*").child_window(control_type="Edit", auto_id="T1").exists():
            self.dlg.child_window(control_type="ListItem", best_match="Coringa").click_input(button='right')
            self.dlg.child_window(title="DropDown").child_window(title="CMD").select()
            time.sleep(1)
        self.dlg = self.app.window(title_re="Coringa_CCD*")
        self.dlg.print_control_identifiers()
        for cmd in inc.remote_shell_command:
            self.dlg.child_window(auto_id="T2").set_text(cmd)
            self.dlg.type_keys('{ENTER}')
        time.sleep(2)
        self.dlg.close()
        # self.app.top_window().print_control_identifiers()

    @retry_if_fail(inc.rats_retry_times)
    def key_logger(self):
        self.dlg = self.app.window(auto_id="FrmMain")
        self.dlg.set_focus()
        self.dlg.child_window(control_type="ListItem", title_re="Coringa*").wait('exists', timeout=120)
        time.sleep(2)
        while not self.app.top_window().child_window(control_type="Edit", auto_id="T1").exists():
            self.dlg.child_window(control_type="ListItem", best_match="Coringa").click_input(button='right')
            self.dlg.child_window(title="DropDown").child_window(title="Keylogger").select()
            time.sleep(1)
        self.dlg = self.app.top_window()
        self.dlg.Edit.type_keys(inc.keylogger_command)
        time.sleep(1)
        self.dlg.close()
        # self.

    @retry_if_fail(inc.rats_retry_times)
    def download_execute(self):
        self.dlg = self.app.window(auto_id="FrmMain")
        self.dlg.set_focus()
        self.dlg.child_window(control_type="ListItem", title_re="Coringa*").wait('exists', timeout=120)

        self.dlg.child_window(control_type="ListItem", best_match="Coringa").click_input(button='right')
        self.dlg.child_window(title="DropDown").child_window(title="Executar Arquivos").select()
        self.dlg.child_window(title="DropDown").child_window(title="Executar Arquivos").child_window(title="URL").click_input()
        time.sleep(1)
        self.dlg = self.app.top_window()
        self.dlg.child_window(auto_id="FrmEnviarURL").child_window(auto_id="TextBox1").set_text(inc.download_and_execute_file_url)
        self.dlg.child_window(auto_id="FrmEnviarURL").Enviar.click()
        time.sleep(1)
        # self.app.top_window().print_control_identifiers()

    def close_software(self):
        try:
            self.app.kill()
        except:
            os.system("taskkill /F /PID {}".format(self.app.process))
        self.server.terminate()
        os.system("taskkill /F /IM Servidor.exe")
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))



    # def start_test(self):
    #     self.remote_shell()
    #     self.key_logger()
    #     self.download_execute()
    #     self.remote_desktop()
    #     self.close_software()

if __name__ == '__main__':
    os.chdir(os.path.abspath(os.path.join(os.getcwd(), "../resources/TA1-Binaries-master")))
    rat = CoringaRat()
    rat.start_test(False, True, True, True)