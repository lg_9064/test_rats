from ratClass.rat import Rat
from config import inc
from pywinauto.application import Application
from utils.phf_operation import retry_if_fail
from utils import get_logger
import subprocess
import threading
import time
import os
import traceback

logger = get_logger.get_common_logger()

class VanToMRat(Rat):
    version = "1.4"

    def __init__(self):
        logger.info("start testing {}".format(self.__class__.__name__))
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        self.server_start_time = time.time()
        self.server = subprocess.Popen(os.path.abspath(r"resources\Alibaba-Cloud-Guard-RATs\Rat\VanToM RAT 1.4\Server.exe"), shell=False)
        self.server_pid = self.server.pid
        logger.debug("vantom rat server pid: {}".format(self.server.pid))
        self.app = Application(backend="uia").start(os.path.abspath(r'resources\Alibaba-Cloud-Guard-RATs\Rat\VanToM RAT 1.4\VanToM RAT 1.4.exe'))
        self.app.window(title="VanToM RAT 1.4").wait('exists', timeout=60)
        self.app_pid = self.app.process
        self.dlg = self.app.window(title="VanToM RAT 1.4")

    def server_start(self):
        self.dlg = self.app.window(title="VanToM RAT 1.4")
        self.dlg.child_window(title="Port Settings").child_window(control_type="Edit").set_text("6547")
        self.dlg.child_window(title="Port Settings").Start.click_input()
        time.sleep(3)

    @retry_if_fail(inc.rats_retry_times)
    def remote_desktop(self):
        self.dlg = self.app.window(title="VanToM RAT 1.4")
        self.dlg.set_focus()
        self.dlg.child_window(auto_id="L1").ListItem.wait('exists', timeout=60)
        self.dlg.child_window(auto_id="L1").ListItem.click_input(button='right')

        for i in range(2):
            self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        time.sleep(1)
        self.dlg = self.app.top_window()
        time.sleep(inc.remote_desktop_sleep_time)
        self.dlg.close()

    @retry_if_fail(inc.rats_retry_times)
    def remote_shell(self):
        self.dlg = self.app.window(title="VanToM RAT 1.4")
        self.dlg.set_focus()
        self.dlg.child_window(auto_id="L1").ListItem.wait('exists', timeout=60)
        self.dlg.child_window(auto_id="L1").ListItem.click_input(button='right')

        for i in range(2):
            self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        for i in range(5):
            self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        time.sleep(1)
        try:
            self.dlg = self.app.window(auto_id="rs0")
            for cmd in inc.remote_shell_command:
                self.dlg.child_window(auto_id="T2").set_text(cmd)
                self.dlg.type_keys('{ENTER}')
            time.sleep(2)
            self.dlg.close()
        except:
            traceback.format_exc()
            self.dlg = self.app.top_window()
            for cmd in inc.remote_shell_command:
                self.dlg.child_window(auto_id="T2").set_text(cmd)
                self.dlg.type_keys('{ENTER}')
            time.sleep(2)
            self.dlg.close()

    @retry_if_fail(inc.rats_retry_times)
    def key_logger(self):
        self.dlg = self.app.window(title="VanToM RAT 1.4")
        self.dlg.set_focus()
        self.dlg.child_window(auto_id="L1").ListItem.wait('exists', timeout=60)
        self.dlg.child_window(auto_id="L1").ListItem.click_input(button='right')

        for i in range(3):
            self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        self.dlg.type_keys('{ENTER}')
        time.sleep(1)
        self.dlg = self.app.top_window()
        self.dlg.type_keys(inc.keylogger_command)
        self.dlg.child_window(title="Refresh").click_input()
        time.sleep(1)
        self.dlg.close()

    @retry_if_fail(inc.rats_retry_times)
    def download_execute(self):
        self.dlg = self.app.window(title="VanToM RAT 1.4")
        self.dlg.set_focus()
        self.dlg.child_window(auto_id="L1").ListItem.wait('exists', timeout=60)
        self.dlg.child_window(auto_id="L1").ListItem.click_input(button='right')

        self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        time.sleep(1)
        try:
            self.dlg.child_window(control_type="Edit").set_text(inc.download_and_execute_file_url)
            self.dlg.child_window(auto_id="OKButton").click()
            time.sleep(1)
            self.dlg.child_window(control_type="Edit").set_text(inc.download_and_execute_file_name)
            self.dlg.child_window(auto_id="OKButton").click()
        except:
            self.dlg = self.app.top_window()
            self.dlg.child_window(control_type="Edit").set_text(inc.download_and_execute_file_url)
            self.dlg.child_window(auto_id="OKButton").click()
            time.sleep(1)
            self.dlg.child_window(control_type="Edit").set_text(inc.download_and_execute_file_name)
            self.dlg.child_window(auto_id="OKButton").click()
        time.sleep(5)

    def close_software(self):
        try:
            self.app.kill()
        except:
            os.system("taskkill /F /PID {}".format(self.app.process))
        self.server.terminate()
        os.system("taskkill /F /IM Server.exe")
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))

    # def start_test(self,a,b,c,d):
    #     # self.server_start()
    #     time.sleep(4)
    #     self.remote_desktop()
    #     self.remote_shell()
    #     self.key_logger()
    #     self.download_execute()
    #     self.close_software()

if __name__ == '__main__':
    os.chdir(os.path.abspath(os.path.join(os.getcwd(), "../resources/TA1-Binaries-master")))
    rat = VanToMRat()
    rat.start_test(True, True, True, True)