from ratClass.rat import Rat
from pywinauto.application import Application
import subprocess
import threading
import time

# useless
class DarkCometRat(Rat):
    version = "5.1"

    def __init__(self):
        self.app = Application(backend="uia").start(r'..\resources\Alibaba-Cloud-Guard-RATs\Rat\Dark Comet 5.1\DarkComet.exe')
        time.sleep(2)
        self.dlg.set_focus()
        self.dlg = self.app.window(class_name = "TApplication")
        self.dlg.print_control_identifiers()
        # self.dlg.child_window(control_type = "Pane").child_window(control_type = "List").click_input(button="right")
        self.dlg.click_input(button = "right")
        self.dlg.print_control_identifiers()


    def server_start(self):
        pass

if __name__ == '__main__':
    rat = DarkCometRat()
    rat.server_start()