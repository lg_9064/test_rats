from ratClass.rat import Rat
from pywinauto.application import Application
import subprocess
import threading
import time

# useless
class BackConnectRat(Rat):
    version = "0.5.0"

    def __init__(self):
        # self.server = subprocess.Popen(r"..\resources\Alibaba-Cloud-Guard-RATs\Rat\Back Connect Rat 0.5.0 Demo Version\Back Connect Rat\Server.exe", shell=True)
        self.app = Application(backend="uia").start(r'resources\resources\Alibaba-Cloud-Guard-RATs\Rat\Back Connect Rat 0.5.0 Demo Version\Back Connect Rat\Back Connect.exe')
        self.app_pid = self.app.process
        self.dlg = self.app.window(auto_id="Form12")
        self.dlg.child_window(title = 'Contiune UnRegeristed').click()

    def server_start(self):
        self.dlg = self.app.window(auto_id="Form1")
        self.dlg.child_window(auto_id="ToolStrip1").child_window(title="Control Center").click_input(button="left")
        # self.dlg.child_window(title="Control Center").menu_select("Settings->Connection and ports")
        self.dlg.child_window(auto_id="ToolStrip1").child_window(title = "Control Center").child_window(title = "Settings").select()
        self.dlg.print_control_identifiers()
        self.dlg.child_window(auto_id="ToolStrip1").child_window(title="Control Center").child_window(title="Settings").child_window(title="Location").select()
        # self.dlg.print_control_identifiers()
        # dlg2 = self.dlg.child_window(title="Control Center").child_window(title="Settings")

        # dlg2.print_control_identifiers()
        # self.dlg.child_window(title="Control Center").child_window(title="Settings").child_window(control_type="MenuItem").select()

    def start_test(self):
        self.server_start()

if __name__ == '__main__':
    rat = BackConnectRat()
    rat.start_test()
