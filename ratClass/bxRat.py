from ratClass.rat import Rat
from pywinauto.application import Application
from config import inc
from utils.phf_operation import retry_if_fail, not_implement
from utils import get_logger
import subprocess
import threading
import time
import traceback
import os

logger = get_logger.get_common_logger()

class BXRat(Rat):
    version = "1.5.1"

    def __init__(self):
        logger.info("start testing {}".format(self.__class__.__name__))
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))
        self.server_start_time = time.time()
        self.server = subprocess.Popen(os.path.abspath(r"resources\Alibaba-Cloud-Guard-RATs\Rat\BX RAT v1.0\BX RAT\Server.exe"), shell=False)
        self.server_pid = self.server.pid
        logger.debug("bx rat server pid: {}".format(self.server.pid))
        self.app = Application(backend="uia").start(os.path.abspath(r'resources\Alibaba-Cloud-Guard-RATs\Rat\BX RAT v1.0\BX RAT\BX RAT.exe'))
        # self.app.top_window().print_control_identifiers()

    def server_start(self):
        self.dlg = self.app.window(auto_id="frmMain", control_type="Window")
        self.dlg.set_focus()
        self.dlg.child_window(title="Settings").click_input()
        time.sleep(1)
        self.dlg.type_keys('{DOWN}')
        self.dlg.type_keys('{ENTER}')
        # while True:
        #     try:
        #         self.dlg.menu_select("Settings->Contact ")
        #         break
        #     except Exception as e:
        #         # print (traceback.format_exc())
        #         pass
        self.dlg.child_window(title="Contact").Start.click()
        time.sleep(5)
        self.dlg.child_window(title="Contact").close()

    @retry_if_fail(inc.rats_retry_times)
    def remote_desktop(self):
        self.dlg = self.app.window(auto_id="frmMain",control_type="Window")
        self.dlg.set_focus()
        self.dlg.child_window(control_type="List").ListItem.wait('exists', timeout=120)
        self.dlg.child_window(control_type="List").ListItem.click_input(button='right')
        self.dlg.child_window(control_type="List").ListItem.click_input(button='right')
        self.dlg.child_window(title="DropDown").RemoteDesktop.select()
        self.dlg = self.app.window(auto_id="frmRemoteDesktop")
        self.dlg.child_window(auto_id="panelTop").Start.click()
        time.sleep(inc.remote_desktop_sleep_time)
        self.dlg.child_window(auto_id="panelTop").Stop.click()
        self.dlg.close()

    @retry_if_fail(inc.rats_retry_times)
    def remote_shell(self):
        self.dlg = self.app.window(auto_id="frmMain", control_type="Window")
        self.dlg.set_focus()
        self.dlg.child_window(control_type="List").ListItem.wait('exists', timeout=120)
        self.dlg.child_window(control_type="List").ListItem.click_input(button='right')
        self.dlg.child_window(control_type="List").ListItem.click_input(button='right')
        self.dlg.child_window(title="DropDown").RemoteShell.select()
        self.dlg = self.app.window(auto_id="frmRemoteShell")
        for cmd in inc.remote_shell_command:
            self.dlg.child_window(auto_id="txtConsoleInput").set_text(cmd)
            self.dlg.child_window(auto_id="txtConsoleInput").type_keys('{ENTER}')
            time.sleep(1)
        self.dlg.close()

    @not_implement
    def key_logger(self):
        pass

    @retry_if_fail(inc.rats_retry_times)
    def download_execute(self):

        self.dlg = self.app.window(auto_id="frmMain", control_type="Window")
        self.dlg.set_focus()
        self.dlg.child_window(control_type="List").ListItem.wait('exists', timeout=120)
        self.dlg.child_window(control_type="List").ListItem.click_input(button='right')
        self.dlg.child_window(control_type="List").ListItem.click_input(button='right')
        self.dlg.child_window(title="DropDown").child_window(title="Download / Run").click_input()
        self.dlg.child_window(auto_id="frmDownloadAndExecute").child_window(auto_id="txtURL").set_text(inc.download_and_execute_file_url)
        self.dlg.child_window(auto_id="frmDownloadAndExecute").child_window(auto_id="btnDownloadAndExecute").click()
        time.sleep(5)
        self.dlg.close()

    def close_software(self):
        try:
            self.app.kill()
        except:
            os.system("taskkill /F /PID {}".format(self.app.process))
        self.server.terminate()
        os.system("taskkill /F /IM Server.exe")
        os.system("taskkill /F /IM {}".format(inc.download_and_execute_process_name))

    # def start_test(self):
    #     self.server_start()
    #     self.remote_desktop()
    #     self.remote_shell()
    #     self.download_execute()
    #     self.close_software()


if __name__ == '__main__':
    os.chdir(os.path.abspath(os.path.join(os.getcwd(), "../resources/TA1-Binaries-master")))
    rat = BXRat()
    rat.start_test(True, True, True, True)


