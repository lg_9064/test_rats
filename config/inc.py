
download_and_execute_file_url = "http://192.168.1.128:2373/test.exe"
download_and_execute_process_name = "test.exe"
download_and_execute_file_name = "test.exe"
rats_retry_times = 3
remote_shell_command = ["dir"]
keylogger_command = "dnaod{ENTER}XX{DOWN}KDAS"
output_file_name = "res"

# collector输出的output.out文件所在位置
collect_output_file=r'C:\Users\Administrator\Desktop\20181126\output.out'

remote_desktop_sleep_time = 20

# detector_ssh_config = {
#     'hostname':'10.214.148.128',
#     'port':22,
#     'username':'ligang',
#     'password':'ligang'
# }

detector_api = {
    "remote_desktop": ["CreateCompatibleDC", "CreateCompatibleBitmap", "BitBlt"],
    "remote_shell": ["CreatePipe"],
    "download_and_execute": ["ShellExecuteExW", "URLDownloadToFileW"],
    "keylogger": ["GetKeyState", "GetKeyboardState", "GetAsyncKeyState"]
}

# 测试环境database为aptshield，生产环境为aptshield_dev
mysql_config = {
    'host': '192.168.1.128',
    'port': 3306,
    'user': 'root',
    'password': 'zjulist',
    'database': 'aptshield_dev'
}

rat_list=[
    "alusinusRat",
    "babylonRat",
    "bxRat",
    "coringaRat",
    "ctOsRat",
    "greameRat",
    "imminentRat",
    "imperiumRat",
    "l6Rat",
    "ningaliNetRat",
    "novaLiteRat",
    "quasarRat",
    "revengeRat",
    "spyGateRat",
    "vanToMRat"
        ]