
import requests
import zipfile
import sys
import urllib
import os
import subprocess

base_path = 'resources/'

def download_file(url, path='', file_name=None):
    res = requests.get(url)
    if not file_name:
        file_name = res.headers['Content-Disposition'].split('filename=')[1]
        file_name = file_name.replace('"', '').replace("'", "")
    with open(path+file_name, 'wb') as file:
        file.write(res.content)
    return file_name


def unzip_file(from_path, to_path):
    zip_ref = zipfile.ZipFile(from_path, 'r')
    zip_ref.extractall(to_path)
    zip_ref.close()

def install_vc():
    pass

def run_powershell_scripts():
    p = subprocess.Popen(["C:\\WINDOWS\\system32\\WindowsPowerShell\\v1.0\\powershell.exe", "Set-ExecutionPolicy","RemoteSigned"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    # p.stdin.write(".\\resources\\TA1-Binaries-master\\requirement\\Step1-beforeReboot.ps1".encode())
    p.stdin.write("echo 乱码".encode())
    p.stdin.close()
    p.wait()
    for line in p.stdout.readlines():
        print (line)
    # p = subprocess.check_output(
    #  ["C:\\WINDOWS\\system32\\WindowsPowerShell\\v1.0\\powershell.exe", ".\\resources\\TA1-Binaries-master\\requirement\\Step1-beforeReboot.ps1",])
    # print (p)

def main():
    # if not os.path.exists(base_path+''):
    #     file_name = download_file('http://118.126.94.181/api/v4/projects/21/repository/archive.zip?private_token=tzqQHcDJrkvyYc3xx4KU&ref=master', path=base_path)
    #     unzip_file(file_name, base_path)
    run_powershell_scripts()
    # download_package()
    # download_file('http://118.126.94.181/api/v4/projects/15/repository/archive.zip?private_token=tzqQHcDJrkvyYc3xx4KU', 'test.zip')

if __name__ == '__main__':
    main()