from config import inc
from utils.phf_operation import restore_path
import json
import re
import os
import traceback
import pymysql

def read_data(test_res):
    datas = []
    for line in open(inc.collect_output_file, 'r', encoding='utf-8'):
        try:
            tmp = json.loads(line)
            for each in test_res:
                if (each['pid'] == tmp['processID']):
                    datas.append(json.loads(line))
        except Exception:
            print (line)
            traceback.print_exc()
    return datas
    # datas = []
    # i=0
    # for line in open(inc.collect_output_file, 'r', encoding='utf-8'):
    #     if i==1000:
    #         break
    #     i+=1
    #     datas.append(json.loads(line))
    # print (datas)
    # return datas

@restore_path
def read_format():
    # os.chdir(r'../../')
    formats = {}
    for line in open(r'resources/format.txt', 'r', encoding='utf-8'):
        if re.match(r'([0-9]+)\s+([0-9]+)\s+([A-Za-z0-9]+)', line):
            tmp = line.rstrip('\n').split(' ')
            provider_id = tmp[0]
            opcode = tmp[1]
            name = tmp[2]
            formats["{}_{}".format(provider_id, opcode)] = name
    # print (formats)
    return formats

def match(data, formats):
    res = {}
    for each in data:
        tmp = {}
        thread_id = each['threadID']
        process_id = each['processID']
        # provider_id = each['providerId']
        # opcode = each['opcode']
        # name = formats["{}_{}".format(provider_id, opcode)]
        # print (process_id, thread_id)
        if 'EventName' in each.keys():
            name = each['EventName']
        elif 'CallStack' in each.keys():
            name = each['CallStack']
        else:
            print (each)
            continue

        if process_id in res.keys():
            if thread_id in res[process_id].keys():
                if name not in res[process_id][thread_id]:
                    res[process_id][thread_id].append(name)
            else:
                res[process_id][thread_id] = [name]
        else:
            tmp[thread_id] = [name]
            res[process_id] = tmp
    # print (json.dumps(res, indent=4))
    return res
    # print (res_str)

def get_traces(test_res):
    datas = read_data(test_res)
    formats = read_format()
    return match(datas, formats)

def simple_detect(test_res):
    for each in test_res:
        tmp_apis = {
            "remote_desktop": [],
            "remote_shell": [],
            "download_and_execute": [],
            "keylogger": []
        }
        if 'traces' not in each.keys():
            continue
        for thread in each['traces']:
            for standard_api in inc.detector_api['remote_desktop']:
                if standard_api in each['traces'][thread] and standard_api not in tmp_apis["remote_desktop"]:
                    tmp_apis["remote_desktop"].append(standard_api)
            for standard_api in inc.detector_api['remote_shell']:
                if standard_api in each['traces'][thread] and standard_api not in tmp_apis["remote_shell"]:
                    tmp_apis["remote_shell"].append(standard_api)
            for standard_api in inc.detector_api['download_and_execute']:
                if standard_api in each['traces'][thread] and standard_api not in tmp_apis["download_and_execute"]:
                    tmp_apis["download_and_execute"].append(standard_api)
            for standard_api in inc.detector_api['keylogger']:
                if standard_api in each['traces'][thread] and standard_api not in tmp_apis["keylogger"]:
                    tmp_apis["keylogger"].append(standard_api)
        each["simple_detector_res"] = tmp_apis



# def get_traces(test_res):
#     datas = read_data(test_res)
#     res = {}
#     print (datas)
#     print (test_res)
#     for each in datas:
#         tmp = {}
#         thread_id = each['threadID']
#         process_id = each['processID']
#         for each2 in test_res:
#             if (each2['pid'] == process_id):
#                 if 'EventName' in each.keys():
#                     name = each['EventName']
#                 elif 'CallStack' in each.keys():
#                     name = each['CallStack']
#                 else:
#                     continue
#                 if process_id in res.keys():
#                     if thread_id in res[process_id].keys():
#                         if name not in res[process_id][thread_id]:
#                             res[process_id][thread_id].append(name)
#                     else:
#                         res[process_id][thread_id] = [name]
#                 else:
#                     tmp[thread_id] = [name]
#                     res[process_id] = tmp
#     # print (json.dumps(res, indent=4))
#     return res
#     # print (res_str)
#     # formats = read_format()
#     # return match(datas, formats)

@restore_path
def generate_table(res):
    # os.chdir(r"../../")
    res_html = """
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>result</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">  
	<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<table border="1" class="table table-striped">
    <tr>
        <th>rat name</th>
        <th>pid</th>
        <th>traces</th>
        <th>detector result</th>
        <th>test result</th>
    </tr>
   
    """
    for each in res:
        res_html+="""
    <tr>
        <td>{}</td>
        <td>{}</td>
        <td>{}
        <br>
            <button class="btn btn-default" onclick="showFullTrace({})">show full trace</button>
            <pre id="{}" style="display: none">{}</pre>
        </td>
        <td>{}</td>
        <td>{}</td>
    </tr>
        """.format(
            each['rat_name'],
            each['pid'],
            each['simple_detector_res'] if "simple_detector_res" in each else None,
            each['pid'],
            each['pid'],
            json.dumps(each['traces'], indent=4) if "traces" in each else None,
            each['detector_res'] if "detector_res" in each else None,
            each['result'] if "result" in each else None
        )
    res_html+="""
</table>
</body>
<script>
    function showFullTrace(id) {
        if ($("#"+id).css("display")=="none")
            $("#"+id).show();
        else
            $("#"+id).hide()

    }
</script>
</html>     
    """
    with open("res.html", 'w') as file:
        file.write(res_html)

def get_detector_result(test_res):
    pids = []
    for each in test_res:
        pids.append(each['pid'])
    connect = pymysql.connect(**inc.mysql_config)
    cursor = connect.cursor()
    cursor.execute("""
        SELECT PROCESS_ID, PHF FROM SUSPECT_LIST WHERE PROCESS_ID IN ({});
    """.format(",".join((str(i) for i in pids))))
    res = cursor.fetchall()
    res_dict = {}
    phf_type_dict = get_phf_type(cursor)
    for each in res:
        res_dict[each[0]] = transfer_phf(phf_type_dict, each[1])
    cursor.close()
    connect.close()
    # print (res_dict)
    return res_dict
    # pass

def get_phf_type(cursor):
    cursor.execute("""
        SELECT * FROM PHF_TYPE
    """)
    res = cursor.fetchall()
    res_dict = {}
    for each in res:
        res_dict[each[0]] = each[1]
    return res_dict

def transfer_phf(phf_type_dict, str):
    phf_ids = str.split(',')
    for i in range(len(phf_ids)):
        phf_ids[i] = phf_ids[i].strip()
    res = []
    for phf_id in phf_ids:
        try:
            res.append(phf_type_dict[phf_id])
        except:
            pass
    return res


if __name__ == '__main__':
    # get_traces()
    get_detector_result([{
        "pid":1234
    },{"pid":3333}])