import os
import paramiko

detector_ssh_config = {
    'hostname':'10.214.148.128',
    'port':22,
    'username':'ligang',
    'password':'ligang'
}

detector_user_name = "ligang"

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(**detector_ssh_config)
ssh.exec_command("ps -ef | grep " + detector_user_name + " | grep Detector.jar | grep java | awk '{print $2}' | xargs kill")
os.system("taskkill /F /IM ClientScheduler.exe")
os.system("taskkill /F /IM ETWDataCollectorRedesigned.exe")