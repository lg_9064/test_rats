##环境配置
1.	git clone https://lg_9064@bitbucket.org/lg_9064/test_rats.git
2.	下载resource.zip，解压到项目目录
3.	cd 到项目目录，输入pip install -r requirement.txt命令安装所需的包，推荐使用虚拟环境
##使用
python run.py --all
 
可选参数

Rats:

* --remote_desktop
* --remote_shell
* --keylogger
* --download_and_execute
* --all 运行全部phf

Benign:

* -benign install
* -benign uninstall
* -benign run
* -benign install_and_run 安装后启动程序
##输出
程序结果的json格式会输出到项目目录的res.txt文件中，html形式会输出到result.html页面中，包含rat名，pid，以及phf执行的开始和结束时间
##问题
由于不同机器的运行环境不同，测试rats的过程中可能出现一些不可控的情况，如果整个测试流程被某个rat卡住的话，可以先检查是不是rat本身的兼容性的问题，如果确定该rat无法使用，可以修改config/inc.py文件中的rat_list，将该rat注释掉
