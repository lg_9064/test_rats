from functools import wraps
from utils import get_logger, phf_state
import pickle
import os
import requests
import logging
import traceback
import time

logger = get_logger.get_common_logger()

def restore_path(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        path = os.getcwd()
        print (path)
        res = func(*args, **kwargs)
        os.chdir(path)
        return res
    return wrapper

def not_implement(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        func_name = func.__name__
        logger.warning("{} is not implemented".format(func_name))
        func(*args, **kwargs)
        return phf_state.not_implement
    return wrapper

def retry_if_fail(retry_times):
    def decorate(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            for i in range(1, retry_times):
                try:
                    logger.info("testing {}".format(func.__name__))
                    func(*args, **kwargs)
                    return phf_state.success
                except:
                    traceback.print_exc()
                    logger.debug("Retrying phf {} {} times".format(func.__name__, i+1))
                    if i+1 == retry_times:
                        return phf_state.failed
                    time.sleep(20)
            return phf_state.failed
        return wrapper
    return decorate