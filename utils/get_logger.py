import time
import logging
import logging.config
import yaml
import os

CURRENT_DIRECTORY = os.path.dirname(os.path.realpath(__file__))

time_str = time.strftime("%Y-%m-%d")
logger_config = yaml.load(open(os.path.join(CURRENT_DIRECTORY, 'log_config.yaml')))
for key in logger_config['handlers']:
    if 'filename' in logger_config['handlers'][key]:
        logger_config['handlers'][key]['filename'] = "logs/" + logger_config['handlers'][key]['filename'] + time_str + ".log"
logging.config.dictConfig(config=logger_config)


def get_common_logger():
    return logging.getLogger('commonLogger')
