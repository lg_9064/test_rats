from benignClass.benign import Benign
from pywinauto.application import Application
import time
import os

class PhpStudy(Benign):
    file_name = "phpStudy_6.8.5.8"
    install_path = r"..\resources\phpStudy"

    def install(self):
        self.app = Application(backend="uia").start(r'../resources/software/' + self.file_name)
        time.sleep(1)
        self.dlg = self.app.window(control_type="Window")
        self.dlg.type_keys('{ENTER}')
        self.dlg.child_window(control_type="Edit").set_text(os.path.abspath(os.path.join(os.getcwd(), self.install_path)))
        self.dlg.type_keys('{ENTER}')
        self.dlg.type_keys('{ENTER}')
        self.dlg.child_window(control_type="CheckBox").click_input()
        self.dlg.type_keys('{ENTER}')

    def uninstall(self):
        os.system("rd/s/q {}".format(self.install_path))

    def run(self):
        Application(backend="uia").start(r'../resources/phpStudy/phpStudy2016.exe')


    # def install(self):
    #     self.app = Application(backend="uia").start(r'../resources/software/' + self.file_name)
    #     self.dlg = self.app.window(control_type="Window")
    #     self.dlg.child_window(title="Next >").click()
    #     # time.sleep(1)
    #     # self.dlg = self.app.window(title="Wireshark 2.6.2 64-bit Setup ")
    #
    #
    #     self.dlg.child_window(title="I Agree").click()
    #     # time.sleep(1)
    #     self.dlg.child_window(title="Next >").click()
    #     # time.sleep(1)
    #     # self.dlg = self.app.window(title="Wireshark 2.6.2 64-bit Setup")
    #     self.dlg.type_keys('{ENTER}')
    #     # time.sleep(1)
    #     self.dlg.child_window(control_type="Edit").set_text(os.path.abspath(os.path.join(os.getcwd(), self.install_path)))
    #     self.dlg.child_window(title="Next >").click()
    #     # time.sleep(1)
    #     self.dlg.child_window(control_type="CheckBox").click_input()
    #     self.dlg.child_window(title="Next >").click()
    #     # time.sleep(1)
    #
    #     self.dlg.child_window(title="Install").click()
    #     # self.dlg.type_keys('{ENTER}')
    #     time.sleep(30)
    #     self.app.top_window().print_control_identifiers()

if __name__ == '__main__':
    benign = PhpStudy()
    # benign.install()
    benign.run()
    # benign.uninstall()