from benignClass.benign import Benign
from pywinauto.application import Application
import time
import os

class NotePad(Benign):
    file_name = "npp.7.5.7.Installer.x64"
    install_path = r"..\resources\notepad"

    def install(self):
        self.app = Application(backend="uia").start(r'../resources/software/' + self.file_name)
        time.sleep(2)
        self.dlg = self.app.window(title="Installer Language")
        self.dlg.OK.click()
        time.sleep(1)
        self.dlg = self.app.window(title="Notepad++ v7.5.7 安装")
        self.dlg.child_window(title="下一步(N) >").click()
        time.sleep(1)
        self.dlg = self.app.window(title="Notepad++ v7.5.7 安装 ")
        self.dlg.child_window(title="我接受(I)").click()
        time.sleep(1)
        self.dlg.Edit.set_text(os.path.abspath(os.path.join(os.getcwd(), self.install_path)))
        self.dlg.child_window(title="下一步(N) >").click()
        self.dlg.child_window(title="下一步(N) >").click()
        self.dlg = self.app.window(title="Notepad++ v7.5.7 安装")
        self.dlg.set_focus()
        self.dlg.type_keys('I')
        # self.dlg.child_window(title="安装(I)").click()
        time.sleep(10)
        self.dlg = self.app.window(title="Notepad++ v7.5.7 安装 ")
        self.dlg.child_window(control_type="CheckBox").click_input()
        self.dlg.type_keys('F')
        # self.dlg = self.app.window(title="爱奇艺视频 V6.5.68.5801 正式版 安装向导")
        # self.dlg.Button5.click()
        # time.sleep(1)
        # full_path = os.path.abspath(os.path.join(os.getcwd(), self.install_path))
        # print (full_path)
        # self.dlg.Edit.set_text(full_path)
        # self.dlg.child_window(title="立即安装").click()
        # time.sleep(20)
        # self.app.top_window().print_control_identifiers()

    def uninstall(self):
        os.system("rd/s/q {}".format(self.install_path))

    def run(self):
        Application(backend="uia").start(r'../resources/notepad/notepad++.exe')

if __name__ == '__main__':
    benign = NotePad()
    # benign.install()
    benign.run()
    # benign.uninstall()