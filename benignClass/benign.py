from abc import abstractmethod

class Benign():
    @abstractmethod
    def install(self):
        pass

    @abstractmethod
    def uninstall(self):
        pass

    @abstractmethod
    def run(self):
        pass