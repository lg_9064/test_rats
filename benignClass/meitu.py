from benignClass.benign import Benign
from pywinauto.application import Application
import time
import os

class Meitu(Benign):
    file_name = "美图秀秀_4.0.1.2002.bd"
    install_path = r"..\resources\meitu"

    def install(self):
        self.app = Application(backend="uia").start(r'../resources/software/' + self.file_name)
        self.dlg = self.app.window(title="美图秀秀 4.0.1  安装")
        time.sleep(5)
        self.dlg.child_window(title="立即安装美图秀秀 >").click()
        time.sleep(1)
        try:
            self.dlg.RadioButton.click_input()
            self.dlg.child_window(title="下一步(N) >").click()
            time.sleep(1)
        except:
            pass
        self.dlg = self.app.window(title="美图秀秀 4.0.1  安装 ")
        self.dlg.child_window(control_type="Edit").set_text(os.path.abspath(os.path.join(os.getcwd(), self.install_path)))
        self.dlg.child_window(title="安装(I)").click()
        time.sleep(10)
        self.dlg.child_window(title="立即运行美图秀秀 4.0.1").wait('exists', timeout=30)
        self.dlg.child_window(title="立即运行美图秀秀 4.0.1").click_input()
        self.dlg.child_window(title="完成(F)").click()

    def uninstall(self):
        # self.app = Application(backend="uia").start(r'C:\Users\Administrator\PycharmProjects\test_rats\resources\meitu\XiuXiu\uninst')
        # self.app.top_window().type_keys('{ENTER}')
        # self.app.top_window().print_control_identifiers()
        os.system("rd/s/q {}".format(self.install_path))

    def run(self):
        Application(backend="uia").start(r'../resources/meitu/XiuXiu/XiuXiu.exe')

if __name__ == '__main__':
    benign = Meitu()
    # benign.install()
    benign.run()
    # time.sleep(10)
    # benign.uninstall()