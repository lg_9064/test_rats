from benignClass.benign import Benign
from pywinauto.application import Application
import time
import os

class QQLive(Benign):
    file_name = "TencentVideo10.10.2290.0.exe"
    install_path = r"..\resources\qqLive"

    def install(self):
        self.app = Application(backend="uia").start(r'../resources/software/' + self.file_name)
        # self.app.top_window().print_control_identifiers()
        time.sleep(1)
        self.dlg = self.app.window(title="腾讯视频 2018 安装程序 ")
        self.dlg.Button2.click()
        time.sleep(1)
        self.dlg.child_window(control_type="Edit").set_text(os.path.abspath(os.path.join(os.getcwd(), self.install_path)))
        self.dlg.Button4.click()
        self.dlg.Button.wait('exists', timeout=120)
        self.app.kill()
        # self.dlg.checkbox1.click_input()
        # self.dlg.checkbox2.click_input()
        # self.dlg.checkbox3.click_input()
        # self.dlg.checkbox4.click_input()
        # time.sleep(2)
        # self.app
        # self.dlg.child_window(title="虎牙直播，海量蓝光游戏直播、免费无广告", control_type="Button").click_input()
        # self.app.top_window().print_control_identifiers()

    def uninstall(self):
        # self.app = Application(backend="uia").start(os.path.join(self.install_path, "QQLiveUninstaller.exe"))
        # time.sleep(1)
        # self.app.top_window().print_control_identifiers()
        os.system("rd/s/q {}".format(self.install_path))
        # os.removedirs(self.install_path)

    def run(self):
        Application(backend="uia").start(r'../resources/qqLive/qqLive.exe')

if __name__ == '__main__':
    benign = QQLive()
    # benign.install()
    benign.run()
    # benign.uninstall()