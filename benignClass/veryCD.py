from benignClass.benign import Benign
from pywinauto.application import Application
import time
import os

class VeryCD(Benign):
    file_name = "电驴_1.2.2.exe"
    install_path = r"..\resources\veryCD"

    def install(self):
        self.app = Application(backend="uia").start(r'../resources/software/' + self.file_name)
        self.dlg = self.app.window(title="VeryCD 电驴 1.2.2 Build 111110 安装")
        time.sleep(1)
        self.dlg.child_window(title="下一步(N) >").click()
        time.sleep(1)
        self.dlg = self.app.window(title="VeryCD 电驴 1.2.2 Build 111110 安装 ")
        self.dlg.child_window(title="我接受(I)").click()
        time.sleep(1)
        self.dlg.child_window(title="建立快捷方式").click_input()
        self.dlg.child_window(title="增强IE右键菜单").click_input()
        self.dlg.child_window(title="将上网导航设为主页").click_input()
        self.dlg.child_window(title="下一步(N) >").click()
        time.sleep(1)
        self.dlg.child_window(control_type="Edit").set_text(os.path.abspath(os.path.join(os.getcwd(), self.install_path)))
        self.dlg.child_window(title="安装(I)").click()
        time.sleep(10)
        self.dlg.child_window(control_type="CheckBox").click_input()
        self.dlg.child_window(title="完成(F)").click()

    def uninstall(self):
        # self.app = Application(backend="uia").start(os.path.join(self.install_path, "Uninstall.exe"))
        # self.app.top_window().print_control_identifiers()
        # time.sleep(1)
        # self.app.top_window().print_control_identifiers()
        os.system("rd/s/q {}".format(self.install_path))
        # os.removedirs(self.install_path)

    def run(self):
        Application(backend="uia").start(r'../resources/veryCD/emule.exe')

if __name__ == '__main__':
    benign = VeryCD()
    benign.install()
    # benign.run()
    # benign.uninstall()