# useless

from utils import get_logger
import subprocess
import threading
import os
import sys
import pywinauto
import zipfile
import requests
import traceback
import ftplib
import logging
import pathlib


logger = get_logger.get_common_logger()

def config_path():
    dir = os.getcwd()+"\\resources\\TA1-Binaries-master"
    os.chdir(dir)

def download_file(url, path='', file_name=None):
    res = requests.get(url)
    if not file_name:
        file_name = res.headers['Content-Disposition'].split('filename=')[1]
        file_name = file_name.replace('"', '').replace("'", "")
    logger.debug("downloading {}".format(file_name))
    with open(os.path.join(path,file_name), 'wb') as file:
        file.write(res.content)
    return file_name

def an_garcode(dir_name):
    os.chdir(dir_name)
    for temp_name in os.listdir('.'):
        try:
            new_name = temp_name.encode('cp437').decode('gbk')
            os.rename(temp_name, new_name)
            temp_name = new_name
        except:
            pass
        if os.path.isdir(temp_name):
            an_garcode(temp_name)
            os.chdir('..')



def unzip_file(from_path, filename, to_path, password = None):
    try:
        logger.debug("unzip {}".format(filename))
        zip_ref = zipfile.ZipFile(os.path.join(from_path, filename), 'r')
        unzip_path = os.path.join(to_path, os.path.splitext(filename)[0])
        if password is not None:
           password = password.encode('utf-8')
        zip_ref.extractall(unzip_path, pwd=password)
        cwd = os.getcwd()
        # 解决文件名中文乱码问题
        an_garcode(unzip_path)
        os.chdir(cwd)
        zip_ref.close()
    except Exception as e:
        logger.error(traceback.format_exc())

def download_collector(download_path, unzip_path):
    if not os.path.exists(download_path):
        os.makedirs(download_path)
    try:
        file_name = download_file(
            'http://118.126.94.181/api/v4/projects/21/repository/archive.zip?private_token=tzqQHcDJrkvyYc3xx4KU&ref=master',
            path=download_path)
        unzip_file(download_path, file_name, unzip_path)
    except Exception as e:
        logger.error(traceback.format_exc())

def download_rat(download_path, unzip_path):
    if not os.path.exists(download_path):
        os.makedirs(download_path)
    try:
        ftp = ftplib.FTP("10.214.148.129")
        ftp.login("files","files")
        filename = "Alibaba-Cloud-Guard-RATs.zip"
        local_filepath = os.path.join(download_path, filename)
        if not os.path.exists(local_filepath):
            logger.debug("downloading {} from ftp".format(filename))
            with open(local_filepath, "wb") as file:
                ftp.retrbinary("RETR %s" % filename, file.write)
        unzip_file(download_path, filename, unzip_path, "rat")
        ftp.close()
    except Exception as e:
        logger.error(traceback.format_exc())

def download_software(download_path, unzip_path):
    if not os.path.exists(download_path):
        os.makedirs(download_path)
    try:
        ftp = ftplib.FTP("10.214.148.129")
        ftp.login("files", "files")
        ftp.cwd("Softwares/17-网络应用/".encode("utf-8").decode('latin1'))
        filenames = ftp.nlst()
        for filename in filenames:
            local_filepath = os.path.join(download_path, filename)
            if not os.path.exists(local_filepath):
                logger.debug ("downloading {} from ftp".format(filename))
                with open(local_filepath, 'wb') as file:
                    ftp.retrbinary('RETR ' + filename, file.write)
                if os.path.splitext(filename)[-1] == ".zip":
                    unzip_file(download_path, filename, unzip_path)
        ftp.close()
    except Exception as e:
        logger.error(traceback.format_exc())

def download_necessary():
    download_path = 'resources2\\downloads'
    unzip_path = 'resources2\\downloads'
    if not os.path.exists(download_path):
        os.makedirs(download_path)
    logger.info("start download")
    download_software('resources2\\softwares', 'resources2\\softwares')
    logger.info("downloaded softwares")
    download_rat(download_path, 'resources2')
    download_collector(download_path,'resources2')



def main():
    download_necessary()
    # unzip_file('resources2\\softwares', 'WinSCP5133.zip', 'resources2\\softwares')
    # unzip_file('resources2\\softwares','putty2186.zip','resources2\\softwares')
    # download_software('resources/downloads', 'resources/downloads')
    # download_rat('resources/downloads', 'resources')

if __name__ == '__main__':
    main()