from main import main_test
from config import inc
import benign_main
import argparse
import os
import subprocess
import time

parser = argparse.ArgumentParser(description='test rats')

parser.add_argument('--remote_desktop', action="store_true", default=False)
parser.add_argument('--remote_shell', action="store_true", default=False)
parser.add_argument('--keylogger', action="store_true", default=False)
parser.add_argument('--download_and_execute', action="store_true", default=False)
parser.add_argument('--all', action="store_true", default=False, help="run all phf")
parser.add_argument('-f', dest='file', action="store", help="not implement")
parser.add_argument('-benign', dest='benign', action="store", help="benign operation")




def config_test(parse_res):
    # os.chdir("benignClass/")
    if not parse_res.benign:
        test = main_test()
        if parse_res.file:
            test.start_test_with_config_file(parse_res.file)
        elif parse_res.all:
            test.do_test(True, True, True, True)
        else:
            test.do_test(parse_res.remote_desktop, parse_res.remote_shell, parse_res.keylogger, parse_res.download_and_execute)
    else:
        os.chdir("benignClass/")
        if parse_res.benign == "install":
            benign_main.install_all()
        elif parse_res.benign == "uninstall":
            benign_main.uninstall_all()
        elif parse_res.benign == "run":
            benign_main.run_all()
        elif parse_res.benign == "install_and_run":
            benign_main.install_all()
            benign_main.run_all()

def main():
    # run_collector()
    # print (os.getpid())
    config_test(parser.parse_args())
    # time.sleep(100)

if __name__ == "__main__":
    main()