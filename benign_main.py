from benignClass import meitu, notePad, phpStudy, qqLive, veryCD

def get_benign_list():
    benigns = []
    benigns+=[
        meitu.Meitu,
        notePad.NotePad,
        phpStudy.PhpStudy,
        qqLive.QQLive,
        veryCD.VeryCD
    ]
    return benigns

def install_all():
    benigns = get_benign_list()
    for benignClass in benigns:
        benign = benignClass()
        benign.install()


def uninstall_all():
    benigns = get_benign_list()
    for benignClass in benigns:
        benign = benignClass()
        benign.uninstall()


def run_all():
    benigns = get_benign_list()
    for benignClass in benigns:
        benign = benignClass()
        benign.run()



